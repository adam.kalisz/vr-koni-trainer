using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMovement : MonoBehaviour
{
    public float moveSpeed = 5.0f;
    public float rotationSpeed = 5.0f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = 0;
        float verticalInput = 0;

        if (Input.GetKey(KeyCode.Insert)) verticalInput = 1;
        if (Input.GetKey(KeyCode.Delete)) verticalInput = -1;
        if (Input.GetKey(KeyCode.PageUp)) horizontalInput = 1;
        if (Input.GetKey(KeyCode.PageDown)) horizontalInput = -1;
        

        Vector3 movement = new Vector3(horizontalInput, 0, verticalInput) * moveSpeed * Time.deltaTime;

        transform.Translate(movement);
        
        if (Input.GetKey(KeyCode.Home)) 
        {
            // Rotate the object clockwise around the Y-axis
            transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.End)) 
        {
            // Rotate the object counterclockwise around the Y-axis
            transform.Rotate(Vector3.forward * -rotationSpeed * Time.deltaTime);
        }
    }
}
