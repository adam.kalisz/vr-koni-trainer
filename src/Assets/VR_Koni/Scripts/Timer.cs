using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    // ===== public varaibles
    public float time = 0.0f;
    public bool timerRunning = false;
    
    public static event Action<float> onTimeChange;
    // ===== ===== =====
    
    
    // Start is called before the first frame update
    void Start()
    {
        ElectroSnareVR.onFirstTimeCutChanges += SetTimer;
    }
    
    private void OnDisable()
    {
        ElectroSnareVR.onFirstTimeCutChanges -= SetTimer;
    }

    // Update is called once per frame
    void Update()
    {
        manageTimer();
    }

    void SetTimer(ElectroSnareVR.CuttingState cuttingState)
    {
        // fist time cutting => start timer
        if (cuttingState == ElectroSnareVR.CuttingState.HealthyTissue)
        {
            timerRunning = true;
        }
        
        // else possible but in case of the need of differentiation we leave it as if
        if (cuttingState != ElectroSnareVR.CuttingState.HealthyTissue)
        {
            timerRunning = false;
        }
    }
    
    void manageTimer()
    {
        if (timerRunning)
        {
            time += Time.deltaTime;
            onTimeChange?.Invoke(time);
        }
    }
}
