using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Canvas_Volume_Eventhandler : MonoBehaviour
{
    private Transform mCanvas;

    private Transform mText;
    private TextMeshPro mTextComponent;

    // Start is called before the first frame update
    void Start()
    {
        mCanvas = transform.Find("Canvas");//transform.Find("Canvas");
        
        mText = mCanvas.GetChild(0);
        mTextComponent = mText.GetComponent<TextMeshPro>();
        
        ElectroSnareVR.onVolumeChange += UpdateText;
    }

    private void OnDisable()
    {
        ElectroSnareVR.onVolumeChange -= UpdateText;
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateText(double volume)
    {
        mTextComponent.SetText(volume.ToString());
    }
}
