using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Canvas_Gamestate_EventHandler : MonoBehaviour
{
    private Transform mCanvas;

    private Transform mText;
    private TextMeshPro mTextComponent;
    
    
    // Start is called before the first frame update
    void Start()
    {
        mCanvas = transform.Find("Canvas");//transform.Find("Canvas");
        
        mText = mCanvas.GetChild(0);
        mTextComponent = mText.GetComponent<TextMeshPro>();

        GameManager.OnGameStateChanged += UpdateText;
    }
    
    private void OnDisable()
    {
        GameManager.OnGameStateChanged -= UpdateText;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void UpdateText(GameState newState)
    {

        string textToDisplay = "";
        switch (newState)
        {
            case GameState.SelectScreen:
                textToDisplay = "Select";
                break;
            case GameState.Operation:
                textToDisplay = "Operation";
                mTextComponent.color = Color.green;
                break;
            case GameState.Victory:
                textToDisplay = "Victory";
                break;
            case GameState.Loose:
                textToDisplay = "GAME OVER";
                mTextComponent.color = Color.red;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }
        
        mTextComponent.SetText(textToDisplay);
    }
}
