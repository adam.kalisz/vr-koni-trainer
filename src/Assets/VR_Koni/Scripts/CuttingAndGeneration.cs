using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;
using Valve.VR.InteractionSystem;

using DelaunatorSharp;
using DelaunatorSharp.Unity;
using Unity.VisualScripting;
using Unity.XR.CoreUtils;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using Valve.Newtonsoft.Json.Utilities;
using VR.Koni_Scrips;

/// <summary>
/// This script is used to cut the cervix and generate two new objects.
/// The current procedure works like this:
/// </summary>

namespace VR.Koni_Scrips
{
    public class CustomPoint : IPoint
    {
        public double X { get; set; }
        public double Y { get; set; }
    
        public CustomPoint(double x, double y)
        {
            X = x;
            Y = y;
        }
    
    }

    public class CustomVertex
    {
        public Vector2 Point { get; set; }
        public bool IsReflex { get; set; }
        public bool IsConvex { get; set; }
        public CustomVertex PrevVertex { get; set; }
        public CustomVertex NextVertex { get; set; }

        public CustomVertex(Vector2 point)
        {
            Point = point;
        }
    }
    
    public class CustomTriangle
    {
        public CustomVertex Vertex1 { get; set; }
        public CustomVertex Vertex2 { get; set; }
        public CustomVertex Vertex3 { get; set; }

        public CustomTriangle(CustomVertex vertex1, CustomVertex vertex2, CustomVertex vertex3)
        {
            Vertex1 = vertex1;
            Vertex2 = vertex2;
            Vertex3 = vertex3;
        }
    }
    
    /// <summary>
    /// Points have to be orderd CCW !!!
    /// </summary>
    public class EarClippingTriangulator
    {
        // create custom vertex data

       
        // Ear Clipping Algorithm Pseudocode:
        /*
        EarClipping(Polygon):
            triangles = empty list
    
            while Polygon has more than 3 vertices:
                for each vertex in Polygon:
                    if Vertex is an "ear":
                        Add the triangle formed by Vertex and its two neighbors to triangles
                        Remove Vertex from Polygon
                        break  // Start checking for ears again from the beginning
                    end if
                end for
        
            // The remaining Polygon is a triangle
            Add it to triangles
        
            return triangles
        */
        
        // steps:
        // 1: The first step is to store the polygon as a doubly linked list so that you can quickly remove ear tip
        // 2: The second step is to iterate over the vertices and find the ears
        // It is sufficient to consider only reflex vertices in the triangle containment tes
        // Once the initial lists for reflex vertices and ears are constructed, the ears are removed one at a tim
        
        /*
         EarClipping(Polygon):
            triangles = empty list
            
            // Construct doubly linked lists for vertices
            ConstructDoublyLinkedLists(Polygon)
            
            // Initialize lists for reflex vertices and ears
            reflexVertices = list of reflex vertices
            ears = list of ear tips
            
            while Polygon has more than 3 vertices:
                for each vertex in Polygon:
                    if Vertex is an ear:
                        // Add the triangle formed by Vertex and its neighbors to triangles
                        AddTriangleToTriangles(Vertex, PreviousVertex, NextVertex, triangles)
                        
                        // Update the adjacent vertices
                        UpdateAdjacentVertices(PreviousVertex, Vertex, NextVertex, reflexVertices, ears)
                        
                        // Remove Vertex from Polygon
                        RemoveVertexFromPolygon(Vertex)
                        
                        break  // Start checking for ears again from the beginning
                    end if
                end for
            
            // The remaining Polygon is a triangle
            Add it to triangles
            
            return triangles
         */

        /// <summary>
        /// The input must be sorted in CCW
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static List<CustomTriangle> EarClippingTriangulation(List<Vector2> points)
        {
            //The list with triangles the method returns
            List<CustomTriangle> triangles = new List<CustomTriangle>();

            // if points less then three return null
            if (points.Count < 3)
                return null;
            
            // if only three points, we dont need the calculations:
            if (points.Count == 3)
            {
                Vector2 point1 = points[0];
                Vector2 point2 = points[1];
                Vector2 point3 = points[2];

                // Calculate the signed area of the triangle formed by the points
                float area = (point2.x - point1.x) * (point3.y - point1.y) - (point2.y - point1.y) * (point3.x - point1.x);

                if (area < 0)
                {
                    // If the area is negative, reverse the order of the points to make it CCW
                    Vector2 temp = point1;
                    point1 = point3;
                    point3 = temp;
                }

                CustomVertex vertex1 = new CustomVertex(point1);
                CustomVertex vertex2 = new CustomVertex(point2);
                CustomVertex vertex3 = new CustomVertex(point3);

                // Create a CustomTriangle using CustomVertex instances
                triangles.Add(new CustomTriangle(vertex1, vertex2, vertex3));

                return triangles;
            }
            
            
            // Construct doubly linked lists for vertices
            List<CustomVertex> customVertices = new List<CustomVertex>();
            
            // Create custom vertex instances and add them to the list
            foreach (Vector2 point in points)
            {
                CustomVertex customVertex = new CustomVertex(point);
                customVertices.Add(customVertex);
            }
            
            // Link the custom vertices into a doubly linked list
            for (int i = 0; i < customVertices.Count; i++)
            {
                int nextPos = (i + 1) % customVertices.Count;
                int prevPos = (i - 1 + customVertices.Count) % customVertices.Count;

                customVertices[i].PrevVertex = customVertices[prevPos];
                customVertices[i].NextVertex = customVertices[nextPos];
            }
            
            // Initialize lists for reflex vertices and ears
            List<CustomVertex> reflexVertices = new List<CustomVertex>();
            List<CustomVertex> ears = new List<CustomVertex>();

            // iterate through list and check for reflexVertices and ears
            foreach (var vertex in customVertices)
            {
                if (IsReflex(vertex))
                {
                    vertex.IsReflex = true;
                    reflexVertices.Add(vertex);
                }
                else
                {
                    vertex.IsReflex = false;
                }

                if (IsConcave(vertex))
                {
                    vertex.IsConvex = true;
                }
                else
                {
                    vertex.IsConvex = false;
                }
            }
            
            // check for ears
            foreach (var vertex in customVertices)
            {
                if (IsEar(vertex, customVertices))
                {
                    ears.Add(vertex);
                }
            }

            
            // while:
            while (customVertices.Count >= 3)
            {
                // Find and remove an ear vertex
                CustomVertex earVertex = FindAndRemoveEarVertex(ears);

                
                // If an ear vertex was found, add the triangle formed by it to the list of triangles
                if (earVertex != null)
                {
                    
                    CustomVertex prevVertex = earVertex.PrevVertex;
                    CustomVertex nextVertex = earVertex.NextVertex;

                    // Check if the area of the triangle formed by these vertices is positive (CW)
                    float area = (nextVertex.Point.x - prevVertex.Point.x) * (earVertex.Point.y - prevVertex.Point.y) - (nextVertex.Point.y - prevVertex.Point.y) * (earVertex.Point.x - prevVertex.Point.x);

                    // Order vertices based on the area
                    CustomVertex vertex1, vertex2, vertex3;
                    if (area > 0) // Positive area indicates CW order
                    {
                        vertex1 = prevVertex;
                        vertex2 = earVertex;
                        vertex3 = nextVertex;
                    }
                    else // Negative area indicates CCW order
                    {
                        vertex1 = prevVertex;
                        vertex2 = nextVertex;
                        vertex3 = earVertex;
                    }
                    
                    triangles.Add(new CustomTriangle(prevVertex, earVertex, nextVertex));

                    // Update the adjacent vertices
                    prevVertex.NextVertex = nextVertex;
                    nextVertex.PrevVertex = prevVertex;

                    // Check if the updated adjacent vertices are now ears
                    if (IsEar(prevVertex, customVertices))
                    {
                        ears.Add(prevVertex);
                    }
                    if (IsEar(nextVertex, customVertices))
                    {
                        ears.Add(nextVertex);
                    }
                    
                    
                    // After removing the ear vertex from the ears list, remove it from the customVertices list
                    customVertices.Remove(earVertex);
                }
                else
                {
                    // If no ear vertex was found, you may want to break the loop or handle the case differently
                    break;
                }
            }


            return triangles;
        }

        public static CustomVertex FindAndRemoveEarVertex(List<CustomVertex> ears)
        {
            // Iterate through the ear vertices
            for (int i = 0; i < ears.Count; i++)
            {
                CustomVertex currentVertex = ears[i];
                CustomVertex prevVertex = currentVertex.PrevVertex;
                CustomVertex nextVertex = currentVertex.NextVertex;

                // Check if the currentVertex is an ear
                if (IsEar(currentVertex, ears))
                {
                    // Remove the ear vertex from the list of ears
                    ears.RemoveAt(i);
            
                    // Return the ear vertex
                    return currentVertex;
                }
            }

            // If no ear vertex is found, return null
            return null;
        }



        /// <summary>
        /// A reflex vertex
        /// is one for which the interior angle formed by the two edges sharing it is larger than π radians.
        /// </summary>
        /// <returns></returns>
        public static bool IsReflex(CustomVertex vertex)
        {
            // Get the vectors for the edges sharing the vertex
            Vector2 edge1 = vertex.Point - vertex.PrevVertex.Point;
            Vector2 edge2 = vertex.NextVertex.Point - vertex.Point;

            // Calculate the angle between the two edges using the dot product
            float angle = Vector2.SignedAngle(edge1, edge2);

            // Check if the angle is greater than π radians
            return angle > Mathf.PI;
        }

        
        public static bool IsConcave(CustomVertex vertex)
        {
            // Get the vectors for the edges sharing the vertex
            Vector2 edge1 = vertex.Point - vertex.PrevVertex.Point;
            Vector2 edge2 = vertex.NextVertex.Point - vertex.Point;

            // Calculate the angle between the two edges using the dot product
            float angle = Vector2.SignedAngle(edge1, edge2);

            // Check if the angle is less than π radians
            return angle < Mathf.PI;
        }
        
        /// <summary>
        /// The second step is to iterate over the vertices and find the ears. For each vertex V i
        ///and corresponding triangle ⟨V i−1, V i, V i+1⟩, test all other vertices to see if any are inside the triangle; the
        ///    indexing is modulo n, so V n = V 0 and V −1 = V n−1. If none are inside, you have an ea
        /// </summary>
        /// <returns></returns>
        public static bool IsEar(CustomVertex vertex, List<CustomVertex> polygonVertices)
        {
            CustomVertex prevVertex = vertex.PrevVertex;
            CustomVertex nextVertex = vertex.NextVertex;
            
            // Create the triangle formed by vertex and its adjacent vertices
            CustomTriangle testTriangle = new CustomTriangle(prevVertex, vertex, nextVertex);
            
            // Iterate over all other vertices in the linked list
            // Iterate over all other vertices
            foreach (CustomVertex otherVertex in polygonVertices)
            {
                if (otherVertex != prevVertex && otherVertex != vertex && otherVertex != nextVertex)
                {
                    // Check if the otherVertex is inside the testTriangle
                    if (IsInsideCustomTriangle(otherVertex, testTriangle))
                    {
                        // If any vertex is inside the triangle, it's not an ear
                        return false;
                    }
                }
            }

            // If no other vertex is found inside the triangle, it's an ear
            return true;
        }

        public static bool IsInsideCustomTriangle(CustomVertex vertex, CustomTriangle triangle)
        {
            return IsPointInTriangle(vertex.Point, triangle.Vertex1.Point, triangle.Vertex2.Point,
                triangle.Vertex3.Point, out float alpha, out float beta, out float gamma);
        }
        
        /*
        public static List<CustomTriangle> TriangulateConcavePolygon(List<Vector2> points)
        {
            /*
            //The list with triangles the method returns
            List<CustomTriangle> triangles = new List<CustomTriangle>();
    
            // If we just have three points, then we don't have to do all calculations
            if (points.Count == 3)
            {
                // Create CustomVertex instances from Vector2 points
                CustomVertex vertex1 = new CustomVertex(points[0]);
                CustomVertex vertex2 = new CustomVertex(points[1]);
                CustomVertex vertex3 = new CustomVertex(points[2]);
    
                // Create a CustomTriangle using CustomVertex instances
                triangles.Add(new CustomTriangle(vertex1, vertex2, vertex3));
    
                return triangles;
            }
    
            
            
            //Step 1. Store the vertices in a list and we also need to know the next and prev vertex
            List<CustomVertex> vertices = new List<CustomVertex>();
    
            for (int i = 0; i < points.Count; i++)
            {
                vertices.Add(new CustomVertex(points[i]));
            }
    
            for (int i = 0; i < vertices.Count; i++)
            {
                int nextPos = (i + 1) % vertices.Count;
                int prevPos = (i - 1 + vertices.Count) % vertices.Count;
    
                vertices[i].PrevVertex = vertices[prevPos];
                vertices[i].NextVertex = vertices[nextPos];
            }
    
            List<CustomVertex> earVertices = new List<CustomVertex>();
    
            for (int i = 0; i < vertices.Count; i++)
            {
                CheckIfReflexOrConvex(vertices[i]);
            }
    
            for (int i = 0; i < vertices.Count; i++)
            {
                IsVertexEar(vertices[i], vertices, earVertices);
            }
    
            // Step 3. Triangulate!
            while (earVertices.Count > 0)
            {
                // Make a triangle of the first ear
                CustomVertex earVertex = earVertices[0];
    
                CustomVertex earVertexPrev = earVertex.PrevVertex;
                CustomVertex earVertexNext = earVertex.NextVertex;
    
                CustomTriangle newTriangle = new CustomTriangle(earVertex, earVertexPrev, earVertexNext);
    
                triangles.Add(newTriangle);
    
                // Remove the vertex from the lists
                earVertices.Remove(earVertex);
                vertices.Remove(earVertex);
    
                // Update the previous vertex and next vertex
                earVertexPrev.NextVertex = earVertexNext;
                earVertexNext.PrevVertex = earVertexPrev;
    
                // See if we have found a new ear by investigating the two vertices that were part of the ear
                CheckIfReflexOrConvex(earVertexPrev);
                CheckIfReflexOrConvex(earVertexNext);
    
                earVertices.Remove(earVertexPrev);
                earVertices.Remove(earVertexNext);
    
                IsVertexEar(earVertexPrev, vertices, earVertices);
                IsVertexEar(earVertexNext, vertices, earVertices);
            }
    
            //Debug.Log(triangles.Count);
    
            return triangles;
        }
        
        
        
        //Check if a vertex if reflex or convex, and add to appropriate list
        private static void CheckIfReflexOrConvex(CustomVertex v)
        {
            v.IsReflex = false;
            v.IsConvex = false;
    
            // Access X and Z coordinates from the Point property
            Vector2 a = v.PrevVertex.Point;
            Vector2 b = v.Point;
            Vector2 c = v.NextVertex.Point;
    
            if (IsTriangleOrientedClockwise(a, b, c))
            {
                v.IsReflex = true;
            }
            else
            {
                v.IsConvex = true;
            }
        }
    
    
    
        //Check if a vertex is an ear
        private static void IsVertexEar(CustomVertex  v, List<CustomVertex> vertices, List<CustomVertex> earVertices)
        {
            //A reflex vertex cant be an ear!
            if (v.IsReflex)
            {
                return;
            }
    
            // Triangle to check point in triangle
            Vector2 a = v.PrevVertex.Point;
            Vector2 b = v.Point;
            Vector2 c = v.NextVertex.Point;
    
            bool hasPointInside = false;
    
            for (int i = 0; i < vertices.Count; i++)
            {
                // We only need to check if a reflex vertex is inside of the triangle
                if (vertices[i].IsReflex)
                {
                    Vector2 p = vertices[i].Point;
    
                    // This means inside and not on the hull
                    if (IsPointInTriangle(p, a, b, c, out float alpha, out float beta, out float gamma))
                    {
                        hasPointInside = true;
                        break;
                    }
                }
            }
    
            if (!hasPointInside)
            {
                earVertices.Add(v);
            }
        }
        */
        
        public static bool IsTriangleOrientedClockwise(Vector2 a, Vector2 b, Vector2 c)
        {
            float area = (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y);
    
            // If the signed area is negative, the triangle is oriented clockwise
            return area < 0;
        }
        
        public static bool IsPointInTriangle(Vector2 point, Vector2 v0, Vector2 v1, Vector2 v2, out float alpha, out float beta, out float gamma) {
            // Calculate barycentric coordinates
            float d00 = (v1 - v0).sqrMagnitude;
            float d01 = Vector2.Dot(v1 - v0, v2 - v0);
            float d11 = (v2 - v0).sqrMagnitude;
            float d20 = Vector2.Dot(point - v0, v2 - v0);
            float d21 = Vector2.Dot(point - v0, v1 - v0);

            // Calculate barycentric coordinates
            float denom = d00 * d11 - d01 * d01;
            alpha = (d11 * d20 - d01 * d21) / denom;
            beta = (d00 * d21 - d01 * d20) / denom;
            gamma = 1.0f - alpha - beta;

            // Check if the point is inside the triangle
            return (alpha >= 0) && (beta >= 0) && (gamma >= 0);
        }
    }
}



// WORK IN PROGRESS. THIS IS HARD TO READ AND NOT READY FOR THE APPLICATION. DONT BOTHER LOOKING INTO THIS SCRIPT!!!

public class CuttingAndGeneration : MonoBehaviour
{
    [FormerlySerializedAs("elektroschlingeVR")] public ElectroSnareVR electroSnareVR;
    public ComputeShader dilationShader;
    public ComputeShader erosionShader;
    public ComputeShader splittingShader;

    public ComputeShader drawLines;
    
    public ComputeShader countingPixels;
    private ComputeBuffer greenPixelsBuffer;
    
    private bool hotWire = false;
    private bool cutting = false;

    private Transform leftRayCast;
    private Transform rightRayCast;
    private Transform leftRayCast_1;
    private Transform rightRayCast_1;
    private Transform leftRayCast_2;
    private Transform rightRayCast_2;
    private Transform leftRayCast_3;
    private Transform rightRayCast_3;
    private Transform wireTop;


    private List<Vector3> splicingVertices;

    private List<Vector3> newVertices;

    
    
    
    private bool endCutting = false;

    private int dilationKernelIndex;
    private int erosionKernelIndex;
    private int splittingKernelIndex;

    private int drawLinesKernelIndex;
    
    private int countingPixelsKernelIndex;
    
    private bool raycastFireOn = false;
    
    // counting pixel
    int texWidth = 2048;
    int texHeight = 2048;
    
    private static event Action<bool> onCutting;
    
    // ===== Meshes =====
    public GameObject separatedMeshPrefab; // Prefab for the separated meshes
    public GameObject cancerCellsObject;
    public GameObject cervixObject;


    private void Awake()
    {
        // init lists for storing the raycast positions. splicingVertices takes those generating a hit, newVertices takes all
        splicingVertices = new List<Vector3>();
        newVertices = new List<Vector3>();
    }

    // Start is called before the first frame update
    void Start()
    {
        // subsribe to local event
        onCutting += mainCuttingAndGeneration;
        
        electroSnareVR = GetComponent<ElectroSnareVR>();
        
        // subscribe to wire state event
        ElectroSnareVR.onWireStateChange += HandleWireStateChange;

        ElectroSnareVR.onChangingState += HandleCuttingStateChange;

        ElectroSnareVR.onRayCastFire += HandleRayCastFire;
        
        // set shader kernels
        dilationKernelIndex = dilationShader.FindKernel("CSMain");
        erosionKernelIndex = erosionShader.FindKernel("CSMain");
        splittingKernelIndex = splittingShader.FindKernel("CSMain");
        countingPixelsKernelIndex = countingPixels.FindKernel("CSMain");
        drawLinesKernelIndex = drawLines.FindKernel("CSMain");
    }

    // Update is called once per frame
    // only event in update to prevent costly method call in update loop
    void Update()
    {
        // wire cuts, touches the cervix and a raycast hits
        if (hotWire && cutting && raycastFireOn)
        {
            // if the wire didnt cut before, set endCutting true. 
            if (!endCutting)
            {
                Debug.Log("First Time Cutting");
                endCutting = true;
            }

            // sample positions anyway
            samplePositions();
        }
        else if(!hotWire && endCutting) // wire is cold but endCutting was set to true -> user ended cuttign and disabled the wire
        {
            onCutting?.Invoke(endCutting);// fire event that endCutting is true -> now the splicing can begin
        }
    }


    private void samplePositions()
    {
        // add raycastpositions to the vertices list. But only when they are inside the mesh. 
        // From top to bottom -> Left/Right->1->2->3->Top
        // Check wich raycast has a hit, -> sample all positions under this.
        // index +1 because the one that hits is not inside the mesh. Instead we need the hit Transform location 
        
        Vector3 leftHitVertex = electroSnareVR.currentLeftHit.point;
        Vector3 rightHitVertex = electroSnareVR.currentRightHit.point;
        
        // add the hit to the list of all new Vertices: 
        newVertices.Add(leftHitVertex);
        newVertices.Add(rightHitVertex);
        
        // add the hits to the splicingVertices: they define the edge of the cut
        splicingVertices.Add(leftHitVertex);
        splicingVertices.Add(rightHitVertex);
        
        // add every raycast position under the mesh (hit position)
        for (int i = electroSnareVR.leftHitIndex+1; i < electroSnareVR.leftRayCastList.Count; i++)
        {
            Vector3 vertex = electroSnareVR.leftRayCastList[i].position;
            newVertices.Add(vertex);
        }
        for (int i = electroSnareVR.rightHitIndex+1; i < electroSnareVR.rightRayCastList.Count; i++)
        {
            Vector3 vertex = electroSnareVR.rightRayCastList[i].position;
            newVertices.Add(vertex);
        }
    }

    private void mainCuttingAndGeneration(bool cutting)
    {
        // reset endCutting
        endCutting = false;

        
        Debug.Log("End of Cutting. Now begins the fun stuff");

        // determine the two sides of the sliced object. 
        // for that, dispatch a compute shader that determines all uv coordinates and their corresponding vertices
        // "inside" the polygon drawn by the raycasts on the renderTexture.
        
        // TODO: correct hardcoded tex size
        //dilationShader.SetInt("texWidth", 2048);
        //dilationShader.SetInt("texHeight", 2048);
        
        // get second tex from koni material. 
        dilationShader.SetTexture(dilationKernelIndex, "Result", electroSnareVR.textureToColor);
        
        // dispatch shader
        
        // Calculate the dispatch size based on the texture dimensions
        int numThreadsX = Mathf.CeilToInt(2048 / 8.0f);
        int numThreadsY = Mathf.CeilToInt(2048 / 8.0f);
        
        // copy list
        List<Vector2> allThePixel = electroSnareVR.allDrawnPixel;
        
        // sort list, lowest element in allThePixel[0]
        allThePixel.Sort(CompareByYCoordinate);
        
        int allPixelsDrawn = allThePixel.Count;
        Debug.Log("All Pixels: " + allPixelsDrawn.ToString());

        // apply giftwrapping algorithm. Returns a list of hull Vectors2s
        List<Vector2> hull = GiftwrappingAlgorithm(allThePixel, allPixelsDrawn);
        int hullSize = hull.Count;
        
        // draw lines of created convex hull
        
        // transform vector2 to float2
        List<float2> listToBuffer = new List<float2>();
        foreach (var vector in hull)
        {
            float2 f = new float2(vector.x, vector.y);
            listToBuffer.Add(f);
        }
        
        // send listToBuffer to Shader and draw hull
        ComputeBuffer computeBuffer = new ComputeBuffer(listToBuffer.Count, sizeof(float) * 2); // Each float2 is 2 floats
        computeBuffer.SetData(listToBuffer.ToArray()); // Convert the List<float2> to an array and set it on the buffer
        
        drawLines.SetInt("pixelBufferLength",listToBuffer.Count);
        drawLines.SetBuffer(drawLinesKernelIndex, "PixelBuffer", computeBuffer);
        drawLines.SetTexture(drawLinesKernelIndex, "Result", electroSnareVR.textureToColor);
        drawLines.Dispatch(drawLinesKernelIndex, numThreadsX, numThreadsY,1);
        
        
        // initialize the two gameobjects. At this point, they are empty but with a meshfilter and empty mesh
        InitNewGameObjects();
        
        // list with all affected "edge triangles" with the corresponding vertex indices
        List<(int, int)> edgeTriangleVertexIndicesPairList = new List<(int, int)>();
        
        // 
        BuildMeshes(hull, out List<Vector3> vertices, out List<Vector3> normals, out List<Vector2> uvs, out List<int> triangles, out List<Vector3> additionalVertices, edgeTriangleVertexIndicesPairList);

        Debug.Log("Size of edgetriangleetc : " + edgeTriangleVertexIndicesPairList.Count());
        
        // fill inside of polygon with color
        splittingShader.SetInt("texWidth", 2048);
        splittingShader.SetInt("texHeight", 2048);
        splittingShader.SetInt("pixelBufferLength",listToBuffer.Count);
        splittingShader.SetBuffer(splittingKernelIndex, "PixelBuffer", computeBuffer);
        splittingShader.SetTexture(splittingKernelIndex, "Result", electroSnareVR.textureToColor);
        splittingShader.Dispatch(splittingKernelIndex, numThreadsX, numThreadsY, 1);
        computeBuffer.Release();
        
        
        // inside of edgeTriangleVertexIndicesPairList are pairs of triangle indices (starting points of triangles) and a vertexIndex.
        // the idea is:
        // for every mesh:
        // go through the affected triangles, apply delauny triangluation with every vertex inside or intersecting the edge of this triangle.
        
        
        GenerateMeshes(hull, vertices, normals, uvs, triangles, additionalVertices, edgeTriangleVertexIndicesPairList);
    }
    
    private void OnDisable()
    {
        ElectroSnareVR.onWireStateChange -= HandleWireStateChange;

        ElectroSnareVR.onChangingState -= HandleCuttingStateChange;

        onCutting -= mainCuttingAndGeneration;

    }

    // EventHandler
    private void HandleWireStateChange(bool state)
    {
        if(state)
            hotWire = true;
        else
        {
            hotWire = false;
        }
    }

    private void HandleCuttingStateChange(ElectroSnareVR.CuttingState state)
    {
        if(state == ElectroSnareVR.CuttingState.HealthyTissue)
            cutting = true;
        else
        {
            cutting = false;
        }
    }

    private void HandleRayCastFire(bool fire)
    {
        if (fire)
            raycastFireOn = true;
        else
        {
            raycastFireOn = false;
        }
    }
    
    // sort list 
    int CompareByYCoordinate(Vector2 a, Vector2 b)
    {
        if (a.y < b.y)
            return -1;
        else if (a.y > b.y)
            return 1;
        else
            return 0;
    }

    /// <summary>
    /// Initialize p as leftmost point.
    /// Do following while we don’t come back to the first (or leftmost) point.
    /// The next point q is the point, such that the triplet (p, q, r) is counter clockwise for any other point r.
    /// To find this, we simply initialize q as next point, then we traverse through all points.
    /// For any point i, if i is more counter clockwise, i.e., orientation(p, i, q) is counter clockwise, then we update q as i.
    /// Our final value of q is going to be the most counter clockwise point.
    /// next[p] = q (Store q as next of p in the output convex hull).
    /// p = q (Set p as q for next iteration).
    /// </summary>
    /// <param name="points"></param>
    List<Vector2> GiftwrappingAlgorithm(List<Vector2> points, int numberOfPoints)
    {
        if (numberOfPoints < 3) return null;
        
        // hull list
        List<Vector2> hull = new List<Vector2>();
        
        // lowest y
        Vector2 startingPoint = points[0];
        int startingPointIndex = 0;
        
        // lowest point on y (p in literature)
        //Vector2 current = startingPoint;
        int p = startingPointIndex;
        int q = startingPointIndex;
        
        do
        {
            // add current to hulls
            hull.Add(points[p]);
            
            // init next point
            q = (p + 1) % numberOfPoints;
            
            // go through all points
            for (int i = 0; i < numberOfPoints; i++)
            {
                if (isCCW(points[p], points[q], points[i]) == 2)
                    q = i;
            }

            // set q as next point for p
            p = q;

        } while (p != startingPointIndex);

        return hull;
    }

    /// <summary>
    /// 0 -> collinear
    /// 1 -> clockwise
    /// 2 -> counterclockwise
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="r"></param>
    /// <returns></returns>
    private static int isCCW(Vector2 p, Vector2 q, Vector2 r)
    {
        float val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
        
        // collinear 
        if (val == 0) 
            return 0; 
        
        // clock or counterclock wise 
        return (val > 0)? 1: 2; 
    }


    void BuildMeshes(List<Vector2> hull, out List<Vector3> vertices, out List<Vector3> normals, out List<Vector2> uvs, out List<int> triangles, out List<Vector3> additionalVertices, List<(int, int)> edgeTriangleVertexIndicesPairList  )
    {
        // copy information to not modify the orignial
        vertices = electroSnareVR.canvasMesh.vertices.ToList();
        normals = electroSnareVR.canvasMesh.normals.ToList();
        uvs = electroSnareVR.canvasMesh.uv.ToList();
        triangles = electroSnareVR.canvasMesh.triangles.ToList();

        // information to store the additonal vertices that must be added to both meshes
        additionalVertices = new List<Vector3>();

        // TODO: First add all triangles and vertices. After that separate by color.
        
        // Inserts all new mesh information into the "original" mesh
        InsertNewMeshInformation(hull, vertices, normals, uvs, triangles, additionalVertices, edgeTriangleVertexIndicesPairList);
        
    }


    void GenerateMeshes(List<Vector2> hull, List<Vector3> vertices, List<Vector3> normals, List<Vector2> uvs, List<int> triangles, List<Vector3> additionalVertices, List<(int, int)> edgeTriangleVertexIndicesPairList)
    {
        // now seperate all original vertices into the cancerCellObject and the cervixObject
        
        /*
        // Set components
        Mesh m = cancerCellsObject.GetComponent<MeshFilter>().mesh;
        
        // Debug
        m.vertices = vertices.ToArray();
        m.normals = normals.ToArray();
        m.uv = uvs.ToArray();
        m.triangles = triangles.ToArray();
        
        cancerCellsObject.transform.position = electroSnareVR.cervixTransform.position;
        cancerCellsObject.transform.rotation = electroSnareVR.cervixTransform.rotation;
        cancerCellsObject.transform.localScale = electroSnareVR.cervixTransform.localScale;
        
        // Update the mesh
        //m.RecalculateNormals();
        //m.RecalculateBounds();
        
        // set new material
        MeshRenderer cmR = cancerCellsObject.AddComponent<MeshRenderer>();
        Material newM = new Material(Shader.Find("Standard"));
        newM.color = Color.cyan;
        cmR.material = newM;
        */
        
      
        
        
        List<Vector3> cancerVertices = new List<Vector3>();
        List<Vector3> cancerNormals = new List<Vector3>();
        List<Vector2> cancerUVs = new List<Vector2>();
        List<int> cancerTriangles = new List<int>();

        List<int> vertexIndicesCancerMesh = new List<int>();


        List<Vector3> cervixVertices = new List<Vector3>();
        List<Vector3> cervixNormals = new List<Vector3>();
        List<Vector2> cervixUVs = new List<Vector2>();
        List<int> cervixTriangles = new List<int>();

        List<int> vertexIndicesCervixMesh = new List<int>();

        // ===== Divide =====
        
        // Ensure the RenderTexture is active
        RenderTexture.active = electroSnareVR.textureToColor;

        // Create a new Texture2D to store the pixels
        Texture2D texture = new Texture2D(2048, 2048);

        // Read pixels from the RenderTexture into the Texture2D
        texture.ReadPixels(new Rect(0, 0, 2048, 2048), 0, 0);
        texture.Apply(); // Apply changes to the Texture2D

        // Reset the active RenderTexture
        RenderTexture.active = null;

        // store the indices of the original order in the two lists.. necessary for later accessing the correct indices
        int i = 0;
        
        // Now, 'texture' contains the pixel data from the RenderTexture
        
        // iterate over every uv in the BuildMesh
        foreach (var uv in uvs)
        {
            Color pixelColor = texture.GetPixelBilinear(uv.x, uv.y);
            if (pixelColor == Color.red)
            {
                // add corresponding vertex to the list
                cancerVertices.Add(vertices[i]);
                cancerNormals.Add(normals[i]);
                cancerUVs.Add(uvs[i]);
                
                vertexIndicesCancerMesh.Add(i);
                
                // info also to the other colour if in additional Vertices
                if (additionalVertices.Contains(vertices[i]))
                {
                    cervixVertices.Add(vertices[i]);
                    cervixNormals.Add(normals[i]);
                    cervixUVs.Add(uvs[i]);
                    
                    vertexIndicesCervixMesh.Add(i);
                }
            }
            else
            {
                // add corresponding vertex to the list
                cervixVertices.Add(vertices[i]);
                cervixNormals.Add(normals[i]);
                cervixUVs.Add(uvs[i]);
                
                vertexIndicesCervixMesh.Add(i);
                
                // info also to the other colour if in additional Vertices
                if (additionalVertices.Contains(vertices[i]))
                {
                    cancerVertices.Add(vertices[i]);
                    cancerNormals.Add(normals[i]);
                    cancerUVs.Add(uvs[i]);
                    
                    vertexIndicesCancerMesh.Add(i);
                }
            }
            i++;
        }
        
        for (int j = 0; j < triangles.Count; j += 3)
        {
            int vertexIndex0 = triangles[j];
            int vertexIndex1 = triangles[j + 1];
            int vertexIndex2 = triangles[j + 2];

            // Check if all three vertices of the triangle belong to the cancer or cervix mesh
            if (vertexIndicesCancerMesh.Contains(vertexIndex0) && vertexIndicesCancerMesh.Contains(vertexIndex1) && vertexIndicesCancerMesh.Contains(vertexIndex2))
            {
                cancerTriangles.Add(vertexIndicesCancerMesh.IndexOf(vertexIndex0));
                cancerTriangles.Add(vertexIndicesCancerMesh.IndexOf(vertexIndex1));
                cancerTriangles.Add(vertexIndicesCancerMesh.IndexOf(vertexIndex2));
            }
            else if (vertexIndicesCervixMesh.Contains(vertexIndex0) && vertexIndicesCervixMesh.Contains(vertexIndex1) && vertexIndicesCervixMesh.Contains(vertexIndex2))
            {
                cervixTriangles.Add(vertexIndicesCervixMesh.IndexOf(vertexIndex0));
                cervixTriangles.Add(vertexIndicesCervixMesh.IndexOf(vertexIndex1));
                cervixTriangles.Add(vertexIndicesCervixMesh.IndexOf(vertexIndex2));
            }
        }

        
        CircleTriangleCreation(edgeTriangleVertexIndicesPairList, vertices, triangles, additionalVertices, cancerVertices, cancerTriangles, cervixVertices, cervixTriangles);
        


        // Set components
        Mesh cancerMesh = cancerCellsObject.GetComponent<MeshFilter>().mesh;
        Mesh cervixMesh = cervixObject.GetComponent<MeshFilter>().mesh;
        
        cancerMesh.vertices = cancerVertices.ToArray();
        cancerMesh.normals = cancerNormals.ToArray();
        cancerMesh.uv = cancerUVs.ToArray();
        cancerMesh.triangles = cancerTriangles.ToArray();
        
        cervixMesh.vertices = cervixVertices.ToArray();
        cervixMesh.normals = cervixNormals.ToArray();
        cervixMesh.uv = cervixUVs.ToArray();
        cervixMesh.triangles = cervixTriangles.ToArray();

        cancerCellsObject.transform.position = electroSnareVR.cervixTransform.position;
        cancerCellsObject.transform.rotation = electroSnareVR.cervixTransform.rotation;
        cancerCellsObject.transform.localScale = electroSnareVR.cervixTransform.localScale;
        
        cervixObject.transform.position = electroSnareVR.cervixTransform.position;
        cervixObject.transform.rotation = electroSnareVR.cervixTransform.rotation;
        cervixObject.transform.localScale = electroSnareVR.cervixTransform.localScale;
        
        // Update the mesh
        cancerMesh.RecalculateNormals();
        cancerMesh.RecalculateBounds();
        
        cervixMesh.RecalculateNormals();
        cervixMesh.RecalculateBounds();
        
        // set new material
        MeshRenderer cmR = cancerCellsObject.AddComponent<MeshRenderer>();
        Material newM = new Material(Shader.Find("Standard"));
        newM.color = Color.red;
        cmR.material = newM;
        
        // set new material
        MeshRenderer cxR = cervixObject.AddComponent<MeshRenderer>();
        Material newCvxM = new Material(Shader.Find("Standard"));
        newCvxM.color = Color.blue;
        cxR.material = newCvxM;

    }

    void CircleTriangleCreation(List<(int, int)> edgeTriangleVertexIndicesPairList, List<Vector3> vertices, List<int> triangles, List<Vector3> additionalVertices, List<Vector3> cancerVertices, List<int> cancerTriangles, List<Vector3> cervixVertices, List<int> cervixTriangles)
    {
        
        // DEBUG!
        /*
        // for every mesh:
        Debug.Log("Intro to Circle triangulation");
        
        Debug.Log("Complete Size of edgetriangleetc : " + edgeTriangleVertexIndicesPairList.Count());
        
        Debug.Log("Size of vertices complete: " + vertices.Count);
        Debug.Log("Size of triangles: " + triangles.Count);
        
        Debug.Log("additional vertices: " + additionalVertices.Count());
        
        Debug.Log("orig vertices: " + electroSnareVR.canvasMesh.vertices.Count());
            
            // Group the items by triangleIndex
        var groupedPairs = edgeTriangleVertexIndicesPairList.GroupBy(pair => pair.Item1);

        foreach (var group in groupedPairs)
        {
            Debug.Log("Size of group:" + group.Count());
            
            // check if triangleIndex exists in vertices, triangles
            
            int triangleIndex = group.Key;
            
            // Check if triangleIndex is a valid index for vertices and triangles
            if (triangleIndex >= 0 && triangleIndex < triangles.Count / 3) {
                // It's a valid triangle index
                
                int vertexIndex0 = triangles[triangleIndex * 3];
                
                // You can access vertices and triangles using this index
                Vector3 v0 = vertices[vertexIndex0];
                Vector3 v1 = vertices[vertexIndex0 + 1];
                Vector3 v2 = vertices[vertexIndex0 + 2];
            } else {
                // triangleIndex is out of bounds
                Debug.LogError("Invalid triangleIndex: " + triangleIndex.ToString());
            }
        }
        */
        
        // Group the items by triangleIndex
        var groupedPairs = edgeTriangleVertexIndicesPairList.GroupBy(pair => pair.Item1);

        int c = 0;
        
        // iterate through every triangle index in edgeTriangleVertexIndicesPairList
        foreach (var group in groupedPairs)
        {
            int triangleIndex = group.Key; // This is the common triangleIndex shared by the pairs in this group.
            
            List<(Vector3, int)> verticesInSameTriangleCancer = new List<(Vector3, int)>();
            List<Vector3> onlyVerticesInTriangleCancer = new List<Vector3>();

            List<(Vector3, int)> verticesInSameTriangleCervix = new List<(Vector3, int)>();
            List<Vector3> onlyVerticesInTriangleCervix = new List<Vector3>();
            
            // get original vertices from the triangle index
            int startIndex = triangleIndex * 3; // Each triangle uses 3 consecutive vertices
            
            int vertexIndex0 = triangles[startIndex];
            int vertexIndex1 = triangles[startIndex + 1];
            int vertexIndex2 = triangles[startIndex + 2];

            Vector3 v0 = vertices[vertexIndex0];
            Vector3 v1 = vertices[vertexIndex1];
            Vector3 v2 = vertices[vertexIndex2];
            
            // check if original three vertices are inside the cancer or cervix mesh
            if (cancerVertices.Contains(v0))
            {
                verticesInSameTriangleCancer.Add((v0, vertexIndex0));
                onlyVerticesInTriangleCancer.Add(v0);
            }
            
            if (cervixVertices.Contains(v0))
            {
                verticesInSameTriangleCervix.Add((v0, vertexIndex0));
                onlyVerticesInTriangleCervix.Add(v0);
            }
            
            if (cancerVertices.Contains(v1))
            {
                verticesInSameTriangleCancer.Add((v1, vertexIndex1));
                onlyVerticesInTriangleCancer.Add(v1);
            }
            
            if(cervixVertices.Contains(v1))
            {
                verticesInSameTriangleCervix.Add((v1, vertexIndex1));
                onlyVerticesInTriangleCervix.Add(v1);
            }
            
            if (cancerVertices.Contains(v2))
            {
                verticesInSameTriangleCancer.Add((v2, vertexIndex2));
                onlyVerticesInTriangleCancer.Add(v2);
            }
            
            if(cervixVertices.Contains(v2))
            {
                verticesInSameTriangleCervix.Add((v2, vertexIndex2));
                onlyVerticesInTriangleCervix.Add(v2);
            }
            
            // iterate over all additional vertices in the same triangle. Should still be possible to get the indices 
            // over vertices
            foreach (var pair in group)
            {
                // first, take original vertexIndex and take original vertex
                int vertexIndex = pair.Item2; // This is the vertexIndex from the pair.
                
                Vector3 v = vertices[vertexIndex];
                
                // now, search for the "new" vertexIndex in the two seperated meshes
                if (cancerVertices.Contains(v))
                {
                    // add the additional vertics to both of them
                    verticesInSameTriangleCancer.Add((v,cancerVertices.IndexOf(v) ));
                    onlyVerticesInTriangleCancer.Add(v);
                }
                if (cervixVertices.Contains(v))
                {
                    verticesInSameTriangleCervix.Add((v,cervixVertices.IndexOf(v) ));
                    onlyVerticesInTriangleCervix.Add(v);
                }
            }
            
            
            // now in verticesInSameTriangle are all vertices that are in the same orig triangle
            // do delauny for the vertices#
            // now use delaungy to create the triangles

            Debug.Log("Size of Vertices: " + onlyVerticesInTriangleCancer.Count);

            if (c % 5 == 0)
            {
                DebugVertices(onlyVerticesInTriangleCancer, Color.cyan);
            }

            c++;
            
            
            EarClipTriangles(verticesInSameTriangleCancer, onlyVerticesInTriangleCancer, cancerVertices,
                out List<int> triangleIndicesCancer);
            
            //CreateDelaunyTriangles(verticesInSameTriangleCancer, onlyVerticesInTriangleCancer, cancerVertices, out List<int> triangleIndicesCancer);
            cancerTriangles.AddRange(triangleIndicesCancer);
            
            //EarClipTriangles(verticesInSameTriangleCervix, onlyVerticesInTriangleCervix, cervixVertices,
              //  out List<int> triangleIndicesCervix);
            
            //CreateDelaunyTriangles(verticesInSameTriangleCervix, onlyVerticesInTriangleCervix, cervixVertices, out List<int> triangleIndicesCervix);
            //cervixTriangles.AddRange(triangleIndicesCervix);

            /*
            if (onlyVerticesInTriangleCancer.Count() > 2)
            {
                
            }
            */
            /*
            EarClipTriangles(verticesInSameTriangleCervix, onlyVerticesInTriangleCervix, cervixVertices,
                out List<int> triangleIndicesCervix);
            //CreateDelaunyTriangles(verticesInSameTriangleCervix, onlyVerticesInTriangleCervix, cervixVertices, out List<int> triangleIndicesCervix);
            cervixTriangles.AddRange(triangleIndicesCervix);
            */
        }
    }


    void EarClipTriangles(List<(Vector3, int)> verticesAndIndices, List<Vector3> currentVertices, List<Vector3> refVertices,
        out List<int> triangleIndices)
    {
        triangleIndices = new List<int>();
        EarClipTriangulation(currentVertices, out List<int> triangleIndicesOut);
        
        List<int> origTriangleIndices = new List<int>();
        
        // Iterate through the triangle indices from EarClipTriangulation
        foreach (int triangleIndex in triangleIndicesOut)
        {
            // Find the vertex at the given index in the current vertices list
            Vector3 vertex = currentVertices[triangleIndex];

            // Search for this vertex in the verticesAndIndices list to get its original index
            int originalVertexIndex = refVertices.IndexOf(vertex);

            // Add the original vertex index to the list
            origTriangleIndices.Add(originalVertexIndex);
        }

        
        // Reverse the orientation of triangles by grouping into sets of three and reversing each set
        for (int i = 0; i < origTriangleIndices.Count; i += 3)
        {
            // Reverse the order of vertex indices within the triangle
            int temp = origTriangleIndices[i];
            origTriangleIndices[i] = origTriangleIndices[i + 2];
            origTriangleIndices[i + 2] = temp;
        }
        
        
        triangleIndices.AddRange(origTriangleIndices);
        
    }
    
    
    
    void EarClipTriangulation(List<Vector3> vertices, out List<int> triangleIndices)
    {
        triangleIndices = new List<int>();

        // project vertices onto a plane
        ProjectPointsOntoPlane(vertices, out List<Vector2> projectedVertices);
    
        List<Vector2> ccwOrderedVertices = SortCCW(projectedVertices);

        // Perform the ear clipping triangulation
        List<CustomTriangle> triangles = EarClippingTriangulator.EarClippingTriangulation(ccwOrderedVertices);

        // Iterate through the vertices to find their indices
        Dictionary<Vector2, int> vertexIndexMap = new Dictionary<Vector2, int>();
        for (int i = 0; i < vertices.Count; i++)
        {
            vertexIndexMap[projectedVertices[i]] = i;
        }

        // Convert the triangles to a list of vertex indices
        foreach (CustomTriangle triangle in triangles)
        {
            triangleIndices.Add(vertexIndexMap[triangle.Vertex1.Point]);
            triangleIndices.Add(vertexIndexMap[triangle.Vertex2.Point]);
            triangleIndices.Add(vertexIndexMap[triangle.Vertex3.Point]);
        }
        
        CorrectTriangleOrder(triangleIndices, ccwOrderedVertices);
        
        
        
        
        //ReverseList(triangleIndices);
    }

    void CorrectTriangleOrder(List<int> triangleIndices, List<Vector2> vertices)
    {
        for (int i = 0; i < triangleIndices.Count; i += 3)
        {
            int index1 = triangleIndices[i];
            int index2 = triangleIndices[i + 1];
            int index3 = triangleIndices[i + 2];

            Vector2 vertex1 = vertices[index1];
            Vector2 vertex2 = vertices[index2];
            Vector2 vertex3 = vertices[index3];

            // Calculate the signed area of the triangle formed by the vertices
            float area = (vertex2.x - vertex1.x) * (vertex3.y - vertex1.y) -
                         (vertex2.y - vertex1.y) * (vertex3.x - vertex1.x);

            if (area < 0)
            {
                // If the area is negative (CCW order), reverse the order of vertices
                triangleIndices[i] = index1;
                triangleIndices[i + 1] = index3;
                triangleIndices[i + 2] = index2;
            }
            // You can add an "else" block to handle clockwise (CW) ordering if needed.
        }


        for (int j = 0; j < triangleIndices.Count; j += 3)
        {
            int index1 = triangleIndices[j];
            int index2 = triangleIndices[j + 1];
            int index3 = triangleIndices[j + 2];

            Vector2 vertex1 = vertices[index1];
            Vector2 vertex2 = vertices[index2];
            Vector2 vertex3 = vertices[index3];

            if (isCCW(vertex1, vertex2, vertex3) != 2)
            {
                Debug.LogWarning("NOT CCW!");
            }

        }
    }


    public static void ReverseList(List<int> list)
    {
        int left = 0;
        int right = list.Count - 1;

        while (left < right)
        {
            // Swap elements at the left and right indices
            int temp = list[left];
            list[left] = list[right];
            list[right] = temp;

            // Move the indices towards the center
            left++;
            right--;
        }
    }
    
    List<Vector2> SortCCW(List<Vector2> vertices)
    {
        // Find the centroid of the points
        Vector2 centroid = Vector2.zero;
        foreach (Vector2 vertex in vertices)
        {
            centroid += vertex;
        }
        centroid /= vertices.Count;

        // Sort the vertices based on polar angle
        vertices.Sort((a, b) =>
        {
            float angleA = Mathf.Atan2(a.y - centroid.y, a.x - centroid.x);
            float angleB = Mathf.Atan2(b.y - centroid.y, b.x - centroid.x);
            if (angleA < angleB)
                return -1;
            if (angleA > angleB)
                return 1;
            return 0;
        });

        return vertices;
    }
    
    
    void CreateDelaunyTriangles(List<(Vector3, int)> verticesAndIndices, List<Vector3> currentVertices, List<Vector3> refVertices,
        out List<int> triangleIndices)
    {

        triangleIndices = new List<int>();
        CalcDelaunyTriangulation(currentVertices, out List<int> triangleIndicesOut);

        List<int> origTriangleIndices = new List<int>();
        
        // Iterate through the triangle indices from CalcDelaunyTriangulation
        foreach (int triangleIndex in triangleIndicesOut)
        {
            // Find the vertex at the given index in the current vertices list
            Vector3 vertex = currentVertices[triangleIndex];

            // Search for this vertex in the verticesAndIndices list to get its original index
            int originalVertexIndex = refVertices.IndexOf(vertex);

            // Add the original vertex index to the list
            origTriangleIndices.Add(originalVertexIndex);
        }

        /*
        // Reverse the orientation of triangles by grouping into sets of three and reversing each set
        for (int i = 0; i < origTriangleIndices.Count; i += 3)
        {
            // Reverse the order of vertex indices within the triangle
            int temp = origTriangleIndices[i];
            origTriangleIndices[i] = origTriangleIndices[i + 2];
            origTriangleIndices[i + 2] = temp;
        }
        */
        
        triangleIndices.AddRange(origTriangleIndices);
    }
    
    
    
    
    
    
    /// <summary>
    /// Copies original mesh informations, inserts new vertices and triangles, returns the modified mesh information
    /// We have technically three new informations blocks:
    /// 1: The vertices and triangles from the points creating the seam of the cut.
    /// 2: The vertices and triangles from the intersection of the cut with existing triangles
    /// 3: The new vertices generated by the 3d nature of the cut, where the vertices are generated by the position of
    /// the raycast origins of the electrosnare
    /// </summary>
    void InsertNewMeshInformation(List<Vector2> hull, List<Vector3> vertices, List<Vector3> normals, List<Vector2> uvs, List<int> triangles, List<Vector3> additionalVertices, List<(int, int)> edgeTriangleVertexIndicesPairList)
    {
        // insert the mesh information created by the seam - DONE
        InsertHullInformation(hull, vertices, normals, uvs, triangles, additionalVertices, edgeTriangleVertexIndicesPairList);
        
        Debug.Log("Size of edgetriangleetc: " + edgeTriangleVertexIndicesPairList.Count());
  
        //DebugVertices(additionalVertices, Color.magenta);
        
        List<Vector3> Tvertices = new List<Vector3>();
        List<Vector3> Tnormals = new List<Vector3>();
        List<Vector2> Tuvs = new List<Vector2>();
        List<int> Ttriangles = new List<int>();
        
        // insert the mesh information created by the intersections

        List<(int, int)> edgeTriangleVertexIndicesPairList2 = new List<(int, int)>();
        
        InsertIntersectionInformationV2(hull, vertices, normals, uvs, triangles, Tvertices, Tnormals, Tuvs, Ttriangles, additionalVertices, edgeTriangleVertexIndicesPairList2);
        
        edgeTriangleVertexIndicesPairList.AddRange(edgeTriangleVertexIndicesPairList2);
        
        //DebugVertices(Tvertices, Color.black);
        
        vertices.AddRange(Tvertices);
        normals.AddRange(Tnormals);
        uvs.AddRange(Tuvs);
        //triangles.AddRange(Ttriangles);
    }
    
    
    /// <summary>
    /// inserts the mesh information created by the seam
    /// </summary>
    /// <param name="hull"></param>
    /// <param name="vertices"></param>
    /// <param name="normals"></param>
    /// <param name="uvs"></param>
    /// <param name="triangles"></param>
    /// <param name="additionalVertices"></param>
    void InsertHullInformation(List<Vector2> hull, List<Vector3> vertices, List<Vector3> normals, List<Vector2> uvs, List<int> triangles, List<Vector3> additionalVertices, List<(int, int)> edgeTriangleVertexIndicesPairList)
    {
        // create temp triangleList for safe deletion
        List<(int, Vector3)> trianglesToInsertTo = new List<(int, Vector3)>();
        
        // iterate through every Edge point. Currently this is the result of the Giftwrapping Algorithm but it could 
        // later be every point forming a line for more accurate results
        foreach (var hullPixel in hull)
        {
            // ===== generate information out of hit utilizing a dictionary =====
            if (electroSnareVR.hullRaycastHits.TryGetValue(hullPixel, out RaycastHit hit))
            {
                // generate vertex at correct position
                Vector3 vertex = electroSnareVR.cervixTransform.InverseTransformPoint(hit.point);
                
                // copy normal (could potentially not be perfectly accurate but good enough. For maybe better results triangulate with other normals )
                Vector3 normal = hit.normal;
                
                // get UV coordinate of hullPixel
                Vector2 hullUV = new Vector2(hullPixel.x / (2048 - 1), hullPixel.y / (2048 - 1));

                // get triangle index
                int triangleIndex = hit.triangleIndex;
                
                // Add the original triangle index to the list of triangles to delete
                trianglesToInsertTo.Add((triangleIndex, vertex));
                
                int newIndex = vertices.Count;
                vertices.Add(vertex);
                normals.Add(normal);
                uvs.Add(hullUV);

                // list for checking which vertices are new
                additionalVertices.Add(vertex);
                
                edgeTriangleVertexIndicesPairList.Add((triangleIndex, newIndex));
            }
        }
    }


    /// <summary>
    /// creates the indices for triangle via Delauny triangluation
    /// </summary>
    /// <param name="vertices"></param>
    /// <param name="triangleIndices"></param>
    void CalcDelaunyTriangulation(List<Vector3> vertices, out List<int> triangleIndices)
    {
        triangleIndices = new List<int>();

        List<CustomPoint> projectedPoints = new List<CustomPoint>();
        
        // project vertices onto a plane
        ProjectPointsOntoPlane(vertices, out List<Vector2> projectedVertices);
        
        // vertices to cutompoints
        foreach (var v2D in projectedVertices)
        {
            CustomPoint customPoint = new CustomPoint(v2D.x, v2D.y);
            projectedPoints.Add(customPoint);
        }
        
        // Delaunator Instance
        Delaunator delaunator = new Delaunator(projectedPoints.OfType<IPoint>().ToArray());

        triangleIndices = delaunator.Triangles.ToList();
    }

    void ProjectPointsOntoPlane(List<Vector3> vertices, out List<Vector2> projectedVertices)
    {
        projectedVertices = new List<Vector2>();
        
        /*
        // Create a transformation matrix to rotate the vertices
        Matrix4x4 rotationMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 0, rotationAngleZ));
        
        // Define the plane's normal based on the rotated plane
        Vector3 planeNormal = Quaternion.Euler(0, 0, rotationAngleZ) * Vector3.forward;
        */
        // Get the first three vertices to define the plane
        Vector3 v0 = vertices[0];
        Vector3 v1 = vertices[1];
        Vector3 v2 = vertices[2];

        // Calculate two vectors on the plane
        Vector3 vector1 = v1 - v0;
        Vector3 vector2 = v2 - v0;

        // Calculate the plane's normal vector
        Vector3 planeNormal = Vector3.Cross(vector1, vector2).normalized;
        
        foreach (var vertex in vertices)
        {
            /*
            // Rotate the vertex using the rotation matrix
            Vector3 rotatedVertex = rotationMatrix.MultiplyPoint3x4(vertex);

            // Project the rotated vertex onto the rotated plane
            Vector3 projectedVertex = ProjectPointOntoPlane(rotatedVertex, planeNormal);
            */
            // Project the vertex onto the plane
            Vector3 projectedVertex = ProjectPointOntoPlane(vertex, planeNormal);

            // Convert the projected vertex to a 2D Vector2
            Vector2 projectedVertex2D = new Vector2(projectedVertex.x, projectedVertex.y);

            projectedVertices.Add(projectedVertex2D);
        }
    }
    
    // Function to project a point onto a plane
    Vector3 ProjectPointOntoPlane(Vector3 point, Vector3 planeNormal)
    {
        float distance = Vector3.Dot(planeNormal, point);
        return point - distance * planeNormal;
        
        /*
        // Calculate the projection of the point onto the plane
        Vector3 projectedPoint = point - Vector3.Dot(point, planeNormal) * planeNormal;

        return projectedPoint;
        */
    }




    /// <summary>
    /// Iterate through hull, find ALL! intersections between hull edges and triangles. remember intersection with triangle Indices
    /// </summary>
    /// <param name="hull"></param>
    /// <param name="vertices"></param>
    /// <param name="normals"></param>
    /// <param name="uvs"></param>
    /// <param name="triangles"></param>
    /// <param name="Tvertices"></param>
    /// <param name="Tnormals"></param>
    /// <param name="Tuvs"></param>
    /// <param name="Ttriangles"></param>
    /// <param name="additionalVertices"></param>
    /// <param name="edgeTriangleVertexIndicesPairList"></param>
    void InsertIntersectionInformationV2(List<Vector2> hull, List<Vector3> vertices, List<Vector3> normals,
        List<Vector2> uvs, List<int> triangles, List<Vector3> Tvertices, List<Vector3> Tnormals, List<Vector2> Tuvs,
        List<int> Ttriangles, List<Vector3> additionalVertices, List<(int, int)> edgeTriangleVertexIndicesPairList)
    {
        List<int> tempVertexIndices = new List<int>();

        // keep track of intended indices
        int currentNewVertexIndex = vertices.Count;

        // intersection list
        List<Vector2> intersections = new List<Vector2>();

        Vector3 newVertexToAdd0 = new Vector3();
        Vector3 newNormalToAdd0 = new Vector3();

        Vector3 newVertexToAdd1 = new Vector3();
        Vector3 newNormalToAdd1 = new Vector3();

        Vector3 newVertexToAdd2 = new Vector3();
        Vector3 newNormalToAdd2 = new Vector3();

        // iterate every hull edge
        for (int i = 0; i < hull.Count; i++)
        {
            // get hull edge in Pixel Coordinates
            Vector2 startPoint = hull[i];
            Vector2 endPoint = hull[(i + 1) % hull.Count];

            // convert to UV coordinates
            Vector2 startUV = new Vector2(startPoint.x / (2048 - 1), startPoint.y / (2048 - 1));
            Vector2 endUV = new Vector2(endPoint.x / (2048 - 1), endPoint.y / (2048 - 1));


            // iterate through every triangle edge
            for (int j = 0; j < triangles.Count; j += 3)
            {
                // get triangleIndex based on startingVertex. 
                int currentTriangleIndex = j / 3;
                
                // find vertices of original triangle
                int vertex0Index = triangles[j];
                int vertex1Index = triangles[j + 1];
                int vertex2Index = triangles[j + 2];

                // Get the UV coordinates of the vertices
                Vector2 uv0 = uvs[vertex0Index];
                Vector2 uv1 = uvs[vertex1Index];
                Vector2 uv2 = uvs[vertex2Index];

                // get vertices
                Vector3 v0 = vertices[vertex0Index];
                Vector3 v1 = vertices[vertex1Index];
                Vector3 v2 = vertices[vertex2Index];

                // get normals
                Vector3 n0 = normals[vertex0Index];
                Vector3 n1 = normals[vertex1Index];
                Vector3 n2 = normals[vertex2Index];

                // find ALL intersections between hull edge and triangle edge. Between 1 - 3 intersections.
                // remember wich edge: 
                // v0-v1, v0-v2, v1-2
                FindNewIntersectingVertexPositionsV2(uv0, uv1, uv0, uv2, uv1, uv2, startUV, endUV,
                    out Vector2 inP0, out Vector2 inP1, out Vector2 inP2);
                
                // V0-V1
                if (inP0 != Vector2.zero)
                {
                    generateVertex(inP0, ref newVertexToAdd0, ref newNormalToAdd0, uv0, uv1, v0, v1,
                        n0, n1);

                    Tvertices.Add(newVertexToAdd0);
                    Tnormals.Add(newNormalToAdd0);
                    Tuvs.Add(inP0);

                    additionalVertices.Add(newVertexToAdd0);

                    edgeTriangleVertexIndicesPairList.Add((currentTriangleIndex, currentNewVertexIndex));
                    
                    currentNewVertexIndex++;
                }

                if (inP1 != Vector2.zero)
                {
                    generateVertex(inP1, ref newVertexToAdd1, ref newNormalToAdd1, uv0, uv2, v0, v2,
                        n0, n2);

                    Tvertices.Add(newVertexToAdd1);
                    Tnormals.Add(newNormalToAdd1);
                    Tuvs.Add(inP1);

                    additionalVertices.Add(newVertexToAdd1);
                    
                    edgeTriangleVertexIndicesPairList.Add((currentTriangleIndex, currentNewVertexIndex));

                    currentNewVertexIndex++;
                }

                if (inP2 != Vector2.zero)
                {
                    generateVertex(inP2, ref newVertexToAdd2, ref newNormalToAdd2, uv1, uv2, v1, v2,
                        n1, n2);

                    Tvertices.Add(newVertexToAdd2);
                    Tnormals.Add(newNormalToAdd2);
                    Tuvs.Add(inP2);

                    additionalVertices.Add(newVertexToAdd2);
                    
                    edgeTriangleVertexIndicesPairList.Add((currentTriangleIndex, currentNewVertexIndex));

                    currentNewVertexIndex++;
                }
            }
        }
    }


    /// <summary>
    /// Find intersection points on triangle edges
    /// delete triangles sharing an edge with an intersection point
    /// create new triangles ouf of intersection point and existing vertices
    /// </summary>
    /// <param name="hull"></param>
    /// <param name="vertices"></param>
    /// <param name="normals"></param>
    /// <param name="uvs"></param>
    /// <param name="triangles"></param>
    void InsertIntersectionInformation(List<Vector2> hull, List<Vector3> vertices, List<Vector3> normals,
        List<Vector2> uvs, List<int> triangles, List<Vector3> Tvertices, List<Vector3> Tnormals, List<Vector2> Tuvs,
        List<int> Ttriangles, List<Vector3> additionalVertices, List<(int, int)> edgeTriangleVertexIndicesPairList)
    {
        // hull consists of pixel UVs. Recalc them to UV coordinates. Calc the lines of those uv coordinates

        // List to keep track of triangles to delete
        //List<int> trianglesToDelete = new List<int>();
        //List<Vector3> debugMode = new List<Vector3>();

        // TODO: make out
        Vector3 newVertexToAdd1 = new Vector3();
        Vector3 newNormalToAdd1 = new Vector3();

        Vector3 newVertexToAdd2 = new Vector3();
        Vector3 newNormalToAdd2 = new Vector3();

        List<int> tempVertexIndices = new List<int>();
        
        // keep track of intended indices
        int currentNewVertexIndex = vertices.Count;

        // intersection list
        List<Vector2> intersections = new List<Vector2>();

        // iterate every hull edge
        for (int i = 0; i < hull.Count; i++)
        {
            // get hull edge in Pixel Coordinates
            Vector2 startPoint = hull[i];
            Vector2 endPoint = hull[(i + 1) % hull.Count];

            // convert to UV coordinates
            Vector2 startUV = new Vector2(startPoint.x / (2048 - 1), startPoint.y / (2048 - 1));
            Vector2 endUV = new Vector2(endPoint.x / (2048 - 1), endPoint.y / (2048 - 1));


            // iterate through every triangle edge
            for (int j = 0; j < triangles.Count; j += 3)
            {
                // find vertices of original triangle
                int vertex0Index = triangles[j];
                int vertex1Index = triangles[j + 1];
                int vertex2Index = triangles[j + 2];

                // Get the UV coordinates of the vertices
                Vector2 uv0 = uvs[vertex0Index];
                Vector2 uv1 = uvs[vertex1Index];
                Vector2 uv2 = uvs[vertex2Index];

                // find intersections
                intersections = FindNewIntersectingVertexPositions(uv0, uv1, uv0, uv2, uv1, uv2, startUV, endUV,
                    out int cuttingCase);

                // intersections found
                if (intersections.Count == 2 && cuttingCase >= 0)
                {
                    // get vertices
                    Vector3 v0 = vertices[vertex0Index];
                    Vector3 v1 = vertices[vertex1Index];
                    Vector3 v2 = vertices[vertex2Index];

                    // get normals
                    Vector3 n0 = normals[vertex0Index];
                    Vector3 n1 = normals[vertex1Index];
                    Vector3 n2 = normals[vertex2Index];

                    // add new information
                    switch (cuttingCase)
                    {
                        case 0:

                            generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uv0, uv1, v0, v1,
                                n0, n1);

                            Tvertices.Add(newVertexToAdd1);
                            Tnormals.Add(newNormalToAdd1);

                            generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uv0, uv2, v0, v2,
                                n0, n2);

                            Tvertices.Add(newVertexToAdd2);
                            Tnormals.Add(newNormalToAdd2);

                            additionalVertices.Add(newVertexToAdd1);
                            additionalVertices.Add(newVertexToAdd2);

                            // uv
                            Tuvs.AddRange(intersections);

                            
                            // triangles and indices
                            // find both triangle indices
                            FindIntersectedTriangles(vertices, triangles, v0, v1, j, out int c0secondTriangleindex);
                            FindIntersectedTriangles(vertices, triangles, v0, v2, j, out int c0thirdTriangleindex);
                            
                            tempVertexIndices.Add(currentNewVertexIndex);

                            int i000 = j / 3;
                            int i001 = c0secondTriangleindex / 3;
                            
                            edgeTriangleVertexIndicesPairList.Add((i000, currentNewVertexIndex));
                            edgeTriangleVertexIndicesPairList.Add((i001, currentNewVertexIndex));
                            
                            currentNewVertexIndex++;
                            
                            tempVertexIndices.Add(currentNewVertexIndex);
                            
                            int i010 = j / 3;
                            int i011 = c0thirdTriangleindex / 3;
                            edgeTriangleVertexIndicesPairList.Add((i010, currentNewVertexIndex));
                            edgeTriangleVertexIndicesPairList.Add((i011, currentNewVertexIndex));
                            
                            currentNewVertexIndex++;
                            
                            //AddSeamTriangles(cuttingCase, triangles, j, Ttriangles, tempVertexIndices);
                            break;
                        case 1:
                            generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uv0, uv1, v0, v1,
                                n0, n1);

                            Tvertices.Add(newVertexToAdd1);
                            Tnormals.Add(newNormalToAdd1);

                            generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uv1, uv2, v1, v2,
                                n1, n2);

                            Tvertices.Add(newVertexToAdd2);
                            Tnormals.Add(newNormalToAdd2);

                            additionalVertices.Add(newVertexToAdd1);
                            additionalVertices.Add(newVertexToAdd2);

                            // uv
                            Tuvs.AddRange(intersections);

                            
                            
                            // find both triangle indices
                            FindIntersectedTriangles(vertices, triangles, v0, v1, j, out int c1secondTriangleindex);
                            FindIntersectedTriangles(vertices, triangles, v1, v2, j, out int c1thirdTriangleindex);
                            
                            tempVertexIndices.Add(currentNewVertexIndex);
                            int i100 = j / 3;
                            int i101 = c1secondTriangleindex / 3;
                            edgeTriangleVertexIndicesPairList.Add((i100, currentNewVertexIndex));
                            edgeTriangleVertexIndicesPairList.Add((i101, currentNewVertexIndex));
                            currentNewVertexIndex++;
                            
                            
                            tempVertexIndices.Add(currentNewVertexIndex);
                            int i110 = j / 3;
                            int i111 = c1thirdTriangleindex/3;
                            edgeTriangleVertexIndicesPairList.Add((i110, currentNewVertexIndex));
                            edgeTriangleVertexIndicesPairList.Add((i111, currentNewVertexIndex));
                            currentNewVertexIndex++;

                            
                            
                            // triangles: check wich case, add the corresponding vertex indices + new ones
                            //AddSeamTriangles(cuttingCase, triangles, j, Ttriangles, tempVertexIndices);
                            break;
                        case 2:

                            generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uv0, uv2, v0, v2,
                                n0, n2);

                            Tvertices.Add(newVertexToAdd1);
                            Tnormals.Add(newNormalToAdd1);

                            generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uv1, uv2, v1, v2,
                                n1, n2);

                            Tvertices.Add(newVertexToAdd2);
                            Tnormals.Add(newNormalToAdd2);

                            additionalVertices.Add(newVertexToAdd1);
                            additionalVertices.Add(newVertexToAdd2);

                            // uv
                            Tuvs.AddRange(intersections);

                            // find both triangle indices
                            FindIntersectedTriangles(vertices, triangles, v0, v2, j, out int c2secondTriangleindex);
                            FindIntersectedTriangles(vertices, triangles, v1, v2, j, out int c2thirdTriangleindex);
                            
                            tempVertexIndices.Add(currentNewVertexIndex);
                            int i200 = j / 3;
                            int i201 = c2secondTriangleindex/3;
                            edgeTriangleVertexIndicesPairList.Add((i200, currentNewVertexIndex));
                            edgeTriangleVertexIndicesPairList.Add((i201, currentNewVertexIndex));
                            currentNewVertexIndex++;
                            
                            
                            tempVertexIndices.Add(currentNewVertexIndex);
                            int i210 = j / 3;
                            int i211 = c2thirdTriangleindex/3;
                            edgeTriangleVertexIndicesPairList.Add((i210, currentNewVertexIndex));
                            edgeTriangleVertexIndicesPairList.Add((i211, currentNewVertexIndex));
                            currentNewVertexIndex++;

                            
                            // triangles: check wich case, add the corresponding vertex indices + new ones
                            //AddSeamTriangles(cuttingCase, triangles, j, Ttriangles, tempVertexIndices);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }


    /// <summary>
    /// currentIndex is the startingIndex of a triangle
    /// </summary>
    /// <param name="vertices"></param>
    /// <param name="triangles"></param>
    /// <param name="v0"></param>
    /// <param name="v1"></param>
    /// <param name="currentIndex"></param>
    /// <param name="triangleIndex1"></param>
    void FindIntersectedTriangles(List<Vector3> vertices, List<int> triangles,Vector3 v0, Vector3 v1, int currentIndex, out int triangleIndex1)
    {
        triangleIndex1 = -1;
        
        // Find triangles that share two vertices with currentIndex
        for (int i = 0; i < triangles.Count; i += 3)
        {
            if (i != currentIndex)  // Skip the current triangle
            {
                int sharedVertexCount = 0;

                // Count how many vertices currentIndex shares with the current triangle
                for (int j = 0; j < 3; j++)
                {
                    int vertexIndex = triangles[i + j];

                    if (vertexIndex == vertices.IndexOf(v0) || vertexIndex == vertices.IndexOf(v1))
                    {
                        sharedVertexCount++;
                    }
                }

                // get index if it shares both vertices
                if (sharedVertexCount == 2)
                {
                    triangleIndex1 = i;
                    return;
                }
            }
        }
    }
    
    
    void AddSeamTriangles(int cuttingCase, List<int> triangles, int triangleIndex, List<int> Ttriangles, List<int> newVertexIndices)
   {
       switch (cuttingCase)
       {
           case 0:
               // vertex and normals
               // v0, I0, I1
               // I0, V1, V2
               // I1, I0, V2
        
               // first triangle
               Ttriangles.Add(triangles[triangleIndex]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
               
               // second triangle
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
               Ttriangles.Add(triangles[triangleIndex + 2]);

               // third triangle
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
               Ttriangles.Add(triangles[triangleIndex + 1]);
               Ttriangles.Add(triangles[triangleIndex + 2]);
               
               break;
           case 1:
               // V0, I0, I1
               // I0, V1, I1
               // V0, I1, V2
               
               // first triangle
               Ttriangles.Add(triangles[triangleIndex]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
               
               // second triangle
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
               Ttriangles.Add(triangles[triangleIndex+1]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
               
               // third triangle
               Ttriangles.Add(triangles[triangleIndex]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
               Ttriangles.Add(triangles[triangleIndex+2]);
               
               break;
           case 2:
               // V0, V1, I2
               // V0, I1, I0
               // I0, I1, V2
               
               // first triangle
               Ttriangles.Add(triangles[triangleIndex]);
               Ttriangles.Add(triangles[triangleIndex+1]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
               
               // second triangle
               Ttriangles.Add(triangles[triangleIndex]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
               
               // third triangle
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
               Ttriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
               Ttriangles.Add(triangles[triangleIndex+2]);

               break;
           default:
               break;
       }
   }


       /*
       List<Vector3> vList = new List<Vector3>();
       foreach (var v in debugMode)
       {
           //convert 
           //vList.Add(electroSnareVR.cervixTransform.InverseTransformPoint(v));
       }
       DebugVertices(debugMode);
       */
       // 
       
       
       
       /*
       foreach (var triangleIndex in trianglesToDelete)
       {
           int startIndex = triangleIndex * 3;

           if (triangles.Contains(startIndex))
           {
               Debug.Log("contains");
        
               // Find vertices of the original triangle
               int vertex0Index = triangles[startIndex];
               int vertex1Index = triangles[startIndex + 1];
               int vertex2Index = triangles[startIndex + 2];
        
               // Extract the vertices of the original triangle
               Vector3 vertex0 = vertices[vertex0Index];
               Vector3 vertex1 = vertices[vertex1Index];
               Vector3 vertex2 = vertices[vertex2Index];
        
               bool matchFound = false;

               // Check if the three vertices are all in debugMode
               for (int i = 0; i < debugMode.Count; i += 3)
               {
                   Vector3 debugVertex0 = debugMode[i];
                   Vector3 debugVertex1 = debugMode[i + 1];
                   Vector3 debugVertex2 = debugMode[i + 2];

                   // Check if vertices approximately match
                   if (VerticesApproximatelyEqual(vertex0, debugVertex0) &&
                       VerticesApproximatelyEqual(vertex1, debugVertex1) &&
                       VerticesApproximatelyEqual(vertex2, debugVertex2))
                   {
                       // This triangle should be deleted
                       matchFound = true;
                       break;
                   }
               }

               if (matchFound)
               {
                   // Remove the triangle from the list
                   triangles.Remove(startIndex + 2);
                   triangles.Remove(startIndex + 1);
                   triangles.Remove(startIndex);
               }
           }
       }
       */
       
       //Debug.Log("triangles to delete" + trianglesToDelete.Count);
           
       /*
       // Remove duplicates
       trianglesToDelete = trianglesToDelete.Distinct().ToList();
        
       // Sort the list of original triangle indices to delete in descending order to avoid index shifting issues
       trianglesToDelete.Sort((a, b) => b.CompareTo(a));
        
       Debug.Log("triangles :" + triangles.Count());
       
       // Create a new list of triangles to keep
       
       // Delete triangles from the triangles list
       for (int i = trianglesToDelete.Count - 1; i >= 0; i--)
       {
           int triangleIndex = trianglesToDelete[i];
           int startIndex = triangleIndex * 3;

           // Remove the vertices that belong to the triangle
           triangles.RemoveAt(startIndex + 2);  // Remove vertex2Index
           triangles.RemoveAt(startIndex + 1);  // Remove vertex1Index
           triangles.RemoveAt(startIndex);      // Remove vertex0Index
       }
       */
       
       /*
       trianglesToDelete.Sort((a, b) => b.CompareTo(a));

       bool f = false;
       
       // Delete triangles from the triangles list
       foreach (var triangleIndex in trianglesToDelete)
       {
           int startIndex = (triangleIndex * 3);

           if (triangles.Contains(startIndex) && !f)
           {
               Debug.Log("contains");
               f = true;
               // Remove the vertices that belong to the triangle
               triangles.RemoveAt(startIndex + 2);  // Remove vertex2Index
               triangles.RemoveAt(startIndex + 1);  // Remove vertex1Index
               triangles.RemoveAt(startIndex);      // Remove vertex0Index
           }
       }
       */
       
       
       
       
       
       
       
       
       
       // stores the pixels of the lines
       //List<Vector2> linePixels = new List<Vector2>();
       // determine the pixels of lines
       
       
       
       
       
       
       
       // on ice because indexx problems. Revisit for optimization
       //DetermineLinePixels(hull, vertices, normals, uvs, triangles, linePixels, 20);

    

    // Define a method to compare vertices with tolerance
    bool VerticesApproximatelyEqual(Vector3 v1, Vector3 v2)
    {
        float tolerance = 0.0001f; // Adjust the tolerance as needed
        return Vector3.Distance(v1, v2) < tolerance;
    }
    
    /*
    void CreateAdditionalMeshInfo(List<Vector2> hull, List<Vector3> newVerticesList, List<int> newVertexIndices, List<Vector3> newNormals, List<Vector2> newUVCoordinates, List<int> newTriangles, List<Vector3> triangleVectorList, List<Vector3> hullTriangleList, List<Vector3> hullVertices3DList, List<Vector3> hullVerticesNormalList) 
    {
        int currentNewVertexIndex = 0;//elektroschlingeVR.canvasMesh.vertices.Length;

        // iterate through all existing triangles of the orignial mesh
        for (int triangleIndex = 0; triangleIndex < electroSnareVR.canvasMesh.triangles.Length; triangleIndex+=3)
        {
            // get the uv coordinates of the current original triangle
            Vector2 uvVector0 =  electroSnareVR.canvasMesh.uv[electroSnareVR.canvasMesh.triangles[triangleIndex]];
            Vector2 uvVector1 = electroSnareVR.canvasMesh.uv[electroSnareVR.canvasMesh.triangles[triangleIndex + 1]];
            Vector2 uvVector2 = electroSnareVR.canvasMesh.uv[electroSnareVR.canvasMesh.triangles[triangleIndex + 2]];
            
            // get Vector3D
            Vector3 vt1 = electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex]];
            Vector3 vt2 = electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex + 1]];
            Vector3 vt3 = electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex + 2]];
            
            // get normals
            Vector3 n1 = electroSnareVR.canvasMesh.normals[electroSnareVR.canvasMesh.triangles[triangleIndex]];
            Vector3 n2 = electroSnareVR.canvasMesh.normals[electroSnareVR.canvasMesh.triangles[triangleIndex+1]];
            Vector3 n3 = electroSnareVR.canvasMesh.normals[electroSnareVR.canvasMesh.triangles[triangleIndex+2]];
            
            // get indices
            //int vertexIndex0 = triangleIndices[triangleIndex];
            //int vertexIndex1 = triangleIndices[triangleIndex + 1];
            //int vertexIndex2 = triangleIndices[triangleIndex + 2];

            
            
            
            // set cutting case to -1 and create an empty intersections list
            int cuttingCase = -1;
            List<Vector2> intersections = new List<Vector2>();

            // IMPORTANT: hull has pixel coordinates but we want to check intersections with uv coordinates which 
            // are in the range of 0 - 1
            // check if an edge in hull intersects an triangle (should always be two of three sides of the triangle)
            for (int hullVertexIndex = 0; hullVertexIndex < hull.Count; hullVertexIndex++)
            {
               

                // get edge
                Vector2 startPoint = hull[hullVertexIndex];
                Vector2 endPoint = hull[(hullVertexIndex + 1) % hull.Count];
                
                // normalize edge to fit into uv coordinates
                startPoint.x = startPoint.x / (2048 - 1);
                startPoint.y = startPoint.y / (2048 - 1);
                
                endPoint.x = endPoint.x / (2048 - 1);
                endPoint.y = endPoint.y / (2048 - 1);
                
                // Find intersections between the triangle edges and the cutting edges
                cuttingCase = -1;
                intersections = FindNewIntersectingVertexPositions(uvVector0, uvVector1, uvVector0, uvVector2, uvVector1, uvVector2, startPoint, endPoint, ref cuttingCase);
                
                // create triangles according to the cuttingCase
                if (cuttingCase != -1 && (intersections.Count == 2))
                {
       
                    // add new vertices and uvs to the lists.
                    if (cuttingCase == 0)
                    {
                        // first vertex and normal
                        Vector3 newVertexToAdd1 = new Vector3();
                        Vector3 newNormalToAdd1 = new Vector3();
                        
                        generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uvVector0, uvVector1, vt1, vt2, n1, n2);
                        
                        newVerticesList.Add(newVertexToAdd1);
                        newNormals.Add(newNormalToAdd1);

                        // second vertex and normal
                        Vector3 newVertexToAdd2 = new Vector3();
                        Vector3 newNormalToAdd2 = new Vector3();

                        generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uvVector0, uvVector2, vt1, vt3, n1, n3);
                        
                        newVerticesList.Add(newVertexToAdd2);
                        newNormals.Add(newNormalToAdd2);
                        
                        // vertex indeces
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;

                        // uv
                        newUVCoordinates.AddRange(intersections);
                    
                        // 
                        AddTriangles(cuttingCase, triangleIndex, newVertexIndices, newTriangles, triangleVectorList, newVerticesList);
                    }
                    else if (cuttingCase == 1)
                    {
                        // first vertex and normal
                        Vector3 newVertexToAdd1 = new Vector3();
                        Vector3 newNormalToAdd1 = new Vector3();

                        generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uvVector0, uvVector1, vt1, vt2, n1, n2);
                        
                        newVerticesList.Add(newVertexToAdd1);
                        newNormals.Add(newNormalToAdd1);
                        
                        // second vertex and normal
                        Vector3 newVertexToAdd2 = new Vector3();
                        Vector3 newNormalToAdd2 = new Vector3();
                        
                        generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uvVector1, uvVector2, vt2, vt3, n2, n3);
                        
                        newVerticesList.Add(newVertexToAdd2);
                        newNormals.Add(newNormalToAdd2);
                        
                        
                        // vertex indeces
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;

                        // uv
                        newUVCoordinates.AddRange(intersections);
                    
                        // triangles: check wich case, add the corresponding vertex indices + new ones
                        AddTriangles(cuttingCase, triangleIndex, newVertexIndices, newTriangles, triangleVectorList, newVerticesList);
                    }
                    else if (cuttingCase == 3)
                    {
                        // first vertex and normal
                        Vector3 newVertexToAdd1 = new Vector3();
                        Vector3 newNormalToAdd1 = new Vector3();

                        generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uvVector0, uvVector2, vt1, vt3, n1, n3);
                        
                        newVerticesList.Add(newVertexToAdd1);
                        newNormals.Add(newNormalToAdd1);
                        
                        // second vertex and normal
                        Vector3 newVertexToAdd2 = new Vector3();
                        Vector3 newNormalToAdd2 = new Vector3();
                        
                        generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uvVector1, uvVector2, vt2, vt3, n2, n3);
                        
                        newVerticesList.Add(newVertexToAdd2);
                        newNormals.Add(newNormalToAdd2);

                        // vertex indeces
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;

                        // uv
                        newUVCoordinates.AddRange(intersections);
                    
                        // triangles: check wich case, add the corresponding vertex indices + new ones
                        AddTriangles(cuttingCase, triangleIndex, newVertexIndices, newTriangles, triangleVectorList, newVerticesList);
                    }
                }
            }
        }
    }
    */
    

    void DetermineLinePixels(List<Vector2> hull, List<Vector3> vertices, List<Vector3> normals, List<Vector2> uvs, List<int> triangles, List<Vector2> linePixels, int stepSize)
    {
        // Iterate through hull points and determine pixels from line
        for (int i = 0; i < hull.Count; i++)
        {
            Vector2 startPoint = hull[i];
            Vector2 endPoint = hull[(i + 1) % hull.Count];

            
            DetermineDrawnLine(startPoint, endPoint, linePixels);
            
            /*
            // Determine the direction and length of the line segment
            Vector2 direction = (endPoint - startPoint).normalized;
            float length = (endPoint - startPoint).magnitude;

            // Iterate along the line segment with the specified step size
            for (float t = 0; t <= length; t += stepSize)
            {
                Vector2 pointOnLine = startPoint + direction * t;
                linePixels.Add(pointOnLine);
            }
            */
        }

        
        Debug.Log("linePixels: " + linePixels.Count());
        
        // list for all intersecting triangles
        // use these for deletion later on
        List<int> intersectingTriangles = new List<int>();

        // Now, find the triangles that the line is intersecting
        foreach (Vector2 linePixel in linePixels)
        {
            // Find the triangle that contains the linePixel using your existing code
            int triangleIndex = FindTriangleOfUV(linePixel, vertices, uvs, triangles);

            if (triangleIndex >= 0)
            {
                // triangle found. Ensure it gets added only once
                if(!intersectingTriangles.Contains(triangleIndex))
                    intersectingTriangles.Add(triangleIndex);
            }
        }
        
        Debug.Log("hull points: " + hull.Count());
        Debug.Log("intersecting Triangles found: " + intersectingTriangles.Count());

        /*
        // iterate all intersecting triangles and calc the intersections
        List<Vector2> intersections = new List<Vector2>();

        for (int i = 0; i < hull.Count; i++)
        {
            Vector2 startPoint = hull[i];
            Vector2 endPoint = hull[(i + 1) % hull.Count];
            
            foreach (var triangleIndex in intersectingTriangles)
            {
                // triangle indices
                int vertex0Index = triangles[triangleIndex * 3];
                int vertex1Index = triangles[triangleIndex * 3 + 1];
                int vertex2Index = triangles[triangleIndex * 3 + 2];

                // vertices
                Vector3 vertex0 = vertices[vertex0Index];
                Vector3 vertex1 = vertices[vertex1Index];
                Vector3 vertex2 = vertices[vertex2Index];
            
                // normals
                Vector3 normal0 = normals[vertex0Index];
                Vector3 normal1 = normals[vertex1Index];
                Vector3 normal2 = normals[vertex2Index];
                
                // uvs
                Vector2 uv0 = uvs[vertex0Index];
                Vector2 uv1 = uvs[vertex1Index];
                Vector2 uv2 = uvs[vertex2Index];
            
                // Find intersections between the triangle edges and the cutting edges
                intersections = FindNewIntersectingVertexPositions(uv0, uv1, uv0, uv2, uv1, uv2, startPoint, endPoint, out int cuttingCase);

                // found intersection
                if (intersections.Count() == 2 && cuttingCase >= 0)
                {
                    // generate vertices and triangles
                    switch (cuttingCase)
                    {
                        case 0:
                        {
                            // first vertex and normal
                            Vector3 newVertexToAdd1 = new Vector3();
                            Vector3 newNormalToAdd1 = new Vector3();
                        
                            generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uv0, uv1, vertex0, vertex1, normal0, normal1);
                        
                            vertices.Add(newVertexToAdd1);
                            normals.Add(newNormalToAdd1);

                            // second vertex and normal
                            Vector3 newVertexToAdd2 = new Vector3();
                            Vector3 newNormalToAdd2 = new Vector3();

                            generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uv0, uv2, vertex0, vertex2, normal0, normal2);
                        
                            vertices.Add(newVertexToAdd2);
                            normals.Add(newNormalToAdd2);

                            // uv
                            uvs.AddRange(intersections);
                    
                            // 
                            //AddTriangles(cuttingCase, triangleIndex, newVertexIndices, newTriangles, triangleVectorList, newVerticesList);
                        }
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                        default:
                            
                    }
                }
            }
        }
        */
        
        // delete original triangles
        // Sort the list of original triangle indices to delete in descending order to avoid index shifting issues
        intersectingTriangles.Sort((a, b) => b.CompareTo(a));
        
        // Delete triangles from the triangles list
        foreach (var triangleIndex in intersectingTriangles)
        {
            int startIndex = (triangleIndex * 3);

            // Remove the vertices that belong to the triangle
            triangles.RemoveAt(startIndex + 2);  // Remove vertex2Index
            triangles.RemoveAt(startIndex + 1);  // Remove vertex1Index
            triangles.RemoveAt(startIndex);      // Remove vertex0Index
        }
    }

    void DetermineDrawnLine(Vector2 startPoint, Vector2 endPoint, List<Vector2> linePixels)
    {
        float2 startFloat = new float2(startPoint.x, startPoint.y);
        float2 endFloat= new float2(endPoint.x, endPoint.y);
        
        // Calculate the differences between start and end points
        float2 delta = startFloat - endFloat;
        int2 step = new int2(Math.Sign(delta.x), Math.Sign(delta.y));

        // Ensure deltas are positive
        delta = new float2(Math.Abs(delta.x), Math.Abs(delta.y));

        int2 position = new int2((int)startFloat.x, (int)endFloat.y);

        if (delta.x > delta.y)
        {
            float slope = delta.y / delta.x;
            float error = 0.0f;

            for (int i = 0; i <= (int)delta.x; i++)
            {
                // Store the calculated position in the linePixels list
                linePixels.Add(new Vector2(position.x, position.y));
                
                position.x += step.x;
                error += slope;

                if (error >= 0.5f)
                {
                    position.y += step.y;
                    error -= 1.0f;
                }
            }
        }
        else
        {
            float slope = delta.x / delta.y;
            float error = 0.0f;

            for (int i = 0; i <= (int)delta.y; i++)
            {
                // Store the calculated position in the linePixels list
                linePixels.Add(new Vector2(position.x, position.y));
                
                position.y += step.y;
                error += slope;

                if (error >= 0.5f)
                {
                    position.x += step.x;
                    error -= 1.0f;
                }
            }
        }
    }
    
    
    
    /*
    void DrawLine(float2 start, float2 end)
    {
        // Calculate the differences between start and end points
        float2 delta = end - start;
        int2 step = int2(sign(delta));

        // Ensure deltas are positive
        delta = abs(delta);

        int2 position = int2(start);

        if (delta.x > delta.y)
        {
            float slope = delta.y / delta.x;
            float error = 0.0;

            for (int i = 0; i <= int(delta.x); i++)
            {
                // Set a color or value in the Result texture at the calculated position
                uint2 texCoord = uint2(position.x, position.y);
                Result[texCoord] = float4(1.0, 0.0, 0.0, 1.0);

                position.x += step.x;
                error += slope;

                if (error >= 0.5)
                {
                    position.y += step.y;
                    error -= 1.0;
                }
            }
        }
        else
        {
            float slope = delta.x / delta.y;
            float error = 0.0;

            for (int i = 0; i <= int(delta.y); i++)
            {
                // Set a color or value in the Result texture at the calculated position
                uint2 texCoord = uint2(position.x, position.y);
                Result[texCoord] = float4(1.0, 0.0, 0.0, 1.0); 

                position.y += step.y;
                error += slope;

                if (error >= 0.5)
                {
                    position.x += step.x;
                    error -= 1.0;
                }
            }
        }
    }
    */
    
  
    /*

    /// <summary>
    /// This function generates vertices, vertexindices, normals, uv coordinates and triangles
    /// Used this way: for both of the newly seperated meshes, add the vertices, uv coordinates and normals
    /// For the triangles: check wich orignial vertices are present in the mesh (only check the samller one). Only add
    /// those triangles with complete vertex corners.
    /// Result should be a complete mesh
    /// </summary>
    /// <param name="hull"></param>
    void CreateAdditionalMeshInfo(List<Vector2> hull, List<Vector3> newVerticesList, List<int> newVertexIndices, List<Vector3> newNormals, List<Vector2> newUVCoordinates, List<int> newTriangles, List<Vector3> triangleVectorList, List<Vector3> hullTriangleList, List<Vector3> hullVertices3DList, List<Vector3> hullVerticesNormalList) 
    {
        int currentNewVertexIndex = 0;//elektroschlingeVR.canvasMesh.vertices.Length;

        // iterate through all existing triangles of the orignial mesh
        for (int triangleIndex = 0; triangleIndex < electroSnareVR.canvasMesh.triangles.Length; triangleIndex+=3)
        {
            // get the uv coordinates of the current original triangle
            Vector2 uvVector0 =  electroSnareVR.canvasMesh.uv[electroSnareVR.canvasMesh.triangles[triangleIndex]];
            Vector2 uvVector1 = electroSnareVR.canvasMesh.uv[electroSnareVR.canvasMesh.triangles[triangleIndex + 1]];
            Vector2 uvVector2 = electroSnareVR.canvasMesh.uv[electroSnareVR.canvasMesh.triangles[triangleIndex + 2]];
            
            // get Vector3D
            Vector3 vt1 = electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex]];
            Vector3 vt2 = electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex + 1]];
            Vector3 vt3 = electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex + 2]];
            
            // get normals
            Vector3 n1 = electroSnareVR.canvasMesh.normals[electroSnareVR.canvasMesh.triangles[triangleIndex]];
            Vector3 n2 = electroSnareVR.canvasMesh.normals[electroSnareVR.canvasMesh.triangles[triangleIndex+1]];
            Vector3 n3 = electroSnareVR.canvasMesh.normals[electroSnareVR.canvasMesh.triangles[triangleIndex+2]];
            
            // get indices
            //int vertexIndex0 = triangleIndices[triangleIndex];
            //int vertexIndex1 = triangleIndices[triangleIndex + 1];
            //int vertexIndex2 = triangleIndices[triangleIndex + 2];

            
            
            
            // set cutting case to -1 and create an empty intersections list
            int cuttingCase = -1;
            List<Vector2> intersections = new List<Vector2>();

            // IMPORTANT: hull has pixel coordinates but we want to check intersections with uv coordinates which 
            // are in the range of 0 - 1
            // check if an edge in hull intersects an triangle (should always be two of three sides of the triangle)
            for (int hullVertexIndex = 0; hullVertexIndex < hull.Count; hullVertexIndex++)
            {
                // TODO: REMOVE IF NECESSARY
                // add the triangles of the current hull point to an extra list.
                //CreateHullInformation(hull[hullVertexIndex], uvVector0, uvVector1, uvVector2, vt1, vt2, vt3, n1, n2, n3, hullVertices3DList, hullVerticesNormalList, hullTriangleList);

                // get edge
                Vector2 startPoint = hull[hullVertexIndex];
                Vector2 endPoint = hull[(hullVertexIndex + 1) % hull.Count];
                
                // normalize edge to fit into uv coordinates
                startPoint.x = startPoint.x / (2048 - 1);
                startPoint.y = startPoint.y / (2048 - 1);
                
                endPoint.x = endPoint.x / (2048 - 1);
                endPoint.y = endPoint.y / (2048 - 1);
                
                // Find intersections between the triangle edges and the cutting edges
                cuttingCase = -1;
                intersections = FindNewIntersectingVertexPositions(uvVector0, uvVector1, uvVector0, uvVector2, uvVector1, uvVector2, startPoint, endPoint, ref cuttingCase);
                
                // DEBUG: Overall small distances between intersections
                /*
                if (intersections.Count == 2)
                {
                    var distance = Vector2.Distance(intersections[0], intersections[1]);
                    Debug.Log( "Distance between intersections: " + distance.ToString());
                }
                */
                /*
                // create triangles according to the cuttingCase
                if (cuttingCase != -1 && (intersections.Count == 2))
                {
       
                    // add new vertices and uvs to the lists.
                    if (cuttingCase == 0)
                    {
                        // first vertex and normal
                        Vector3 newVertexToAdd1 = new Vector3();
                        Vector3 newNormalToAdd1 = new Vector3();
                        
                        generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uvVector0, uvVector1, vt1, vt2, n1, n2);
                        
                        newVerticesList.Add(newVertexToAdd1);
                        newNormals.Add(newNormalToAdd1);

                        // second vertex and normal
                        Vector3 newVertexToAdd2 = new Vector3();
                        Vector3 newNormalToAdd2 = new Vector3();

                        generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uvVector0, uvVector2, vt1, vt3, n1, n3);
                        
                        newVerticesList.Add(newVertexToAdd2);
                        newNormals.Add(newNormalToAdd2);
                        
                        // vertex indeces
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;

                        // uv
                        newUVCoordinates.AddRange(intersections);
                    
                        // 
                        AddTriangles(cuttingCase, triangleIndex, newVertexIndices, newTriangles, triangleVectorList, newVerticesList);
                    }
                    else if (cuttingCase == 1)
                    {
                        // first vertex and normal
                        Vector3 newVertexToAdd1 = new Vector3();
                        Vector3 newNormalToAdd1 = new Vector3();

                        generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uvVector0, uvVector1, vt1, vt2, n1, n2);
                        
                        newVerticesList.Add(newVertexToAdd1);
                        newNormals.Add(newNormalToAdd1);
                        
                        // second vertex and normal
                        Vector3 newVertexToAdd2 = new Vector3();
                        Vector3 newNormalToAdd2 = new Vector3();
                        
                        generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uvVector1, uvVector2, vt2, vt3, n2, n3);
                        
                        newVerticesList.Add(newVertexToAdd2);
                        newNormals.Add(newNormalToAdd2);
                        
                        
                        // vertex indeces
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;

                        // uv
                        newUVCoordinates.AddRange(intersections);
                    
                        // triangles: check wich case, add the corresponding vertex indices + new ones
                        AddTriangles(cuttingCase, triangleIndex, newVertexIndices, newTriangles, triangleVectorList, newVerticesList);
                    }
                    else if (cuttingCase == 3)
                    {
                        // first vertex and normal
                        Vector3 newVertexToAdd1 = new Vector3();
                        Vector3 newNormalToAdd1 = new Vector3();

                        generateVertex(intersections[0], ref newVertexToAdd1, ref newNormalToAdd1, uvVector0, uvVector2, vt1, vt3, n1, n3);
                        
                        newVerticesList.Add(newVertexToAdd1);
                        newNormals.Add(newNormalToAdd1);
                        
                        // second vertex and normal
                        Vector3 newVertexToAdd2 = new Vector3();
                        Vector3 newNormalToAdd2 = new Vector3();
                        
                        generateVertex(intersections[1], ref newVertexToAdd2, ref newNormalToAdd2, uvVector1, uvVector2, vt2, vt3, n2, n3);
                        
                        newVerticesList.Add(newVertexToAdd2);
                        newNormals.Add(newNormalToAdd2);

                        // vertex indeces
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;
                        newVertexIndices.Add(currentNewVertexIndex);
                        currentNewVertexIndex++;

                        // uv
                        newUVCoordinates.AddRange(intersections);
                    
                        // triangles: check wich case, add the corresponding vertex indices + new ones
                        AddTriangles(cuttingCase, triangleIndex, newVertexIndices, newTriangles, triangleVectorList, newVerticesList);
                    }
                }
            }
        }
    }
    */




    int FindTriangleOfUV(Vector2 pixelUV, List<Vector3> vertices, List<Vector2> uvs, List<int> triangles)
    {
        int triangleCount = triangles.Count / 3; // Calculate the number of triangles

        for (int i = 0; i < triangleCount; i++)
        {
            int vertex0Index = triangles[i * 3];
            int vertex1Index = triangles[i * 3 + 1];
            int vertex2Index = triangles[i * 3 + 2];

            Vector2 uv0 = uvs[vertex0Index];
            Vector2 uv1 = uvs[vertex1Index];
            Vector2 uv2 = uvs[vertex2Index];

            Vector2 newUV = new Vector2(pixelUV.x / (2048 - 1), pixelUV.y / (2048 - 1));

            // Check if the pixelUV is inside the current triangle based on UV coordinates
            float alpha, beta, gamma;
            if (IsPointInTriangle(newUV, uv0, uv1, uv2, out alpha, out beta, out gamma))
            {
                return i; // Return the triangle index
            }
        }

        return -1;
    }
                
    
    int FindTriangleForVertex(Vector3 vertex, List<Vector3> vertices, List<int> triangles)
    {
        for (int i = 0; i < triangles.Count / 3; i++)
        {
            int vertex0Index = triangles[i * 3];
            int vertex1Index = triangles[i * 3 + 1];
            int vertex2Index = triangles[i * 3 + 2];

            Vector3 vertex0 = vertices[vertex0Index];
            Vector3 vertex1 = vertices[vertex1Index];
            Vector3 vertex2 = vertices[vertex2Index];

            float alpha, beta, gamma;
            if (IsPointInTriangle(vertex, vertex0, vertex1, vertex2, out alpha, out beta, out gamma))
            {
                // The vertex is inside this triangle
                return i;
            }
        }

        // The vertex is not inside any triangle
        return -1;
    }
    
    
    bool IsPointInTriangle(Vector2 point, Vector2 v0, Vector2 v1, Vector2 v2, out float alpha, out float beta, out float gamma) {
        // Calculate barycentric coordinates
        float d00 = (v1 - v0).sqrMagnitude;
        float d01 = Vector2.Dot(v1 - v0, v2 - v0);
        float d11 = (v2 - v0).sqrMagnitude;
        float d20 = Vector2.Dot(point - v0, v2 - v0);
        float d21 = Vector2.Dot(point - v0, v1 - v0);

        // Calculate barycentric coordinates
        float denom = d00 * d11 - d01 * d01;
        alpha = (d11 * d20 - d01 * d21) / denom;
        beta = (d00 * d21 - d01 * d20) / denom;
        gamma = 1.0f - alpha - beta;

        // Check if the point is inside the triangle
        return (alpha >= 0) && (beta >= 0) && (gamma >= 0);
    }
    

    Vector3 CalcNormal(Vector3 normal1, Vector3 normal2, float t)
    {
        return Vector3.Lerp(normal1, normal2, t).normalized;
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="intersection"></param>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <returns></returns>
    void generateVertex(Vector2 intersection, ref Vector3 intersection3D, ref Vector3 normal,Vector2 uvp1, Vector2 uvp2, Vector3 vertex1, Vector3 vertex2, Vector3 normal1, Vector3 normal2)
    {
        // Calculate the direction vector in UV space
        Vector2 uvDirection = uvp2 - uvp1;

        // Calculate the parameter t based on UV space
        float t = Vector2.Dot(intersection - uvp1, uvDirection) / Vector2.Dot(uvDirection, uvDirection);

        // Interpolate the 3D vertex using the parameter t
        intersection3D = Vector3.Lerp(vertex1, vertex2, t);
        normal = CalcNormal(normal1, normal2, t);
    }

    
    
    void AddTriangles(int cuttingCase, int triangleIndex, List<int> newVertexIndices, List<int> newTriangles, List<Vector3> triangleVectorList, List<Vector3> newVertices)
    {
        // TODO: enum and switch case for cutting cases
        // three cases:
        if (cuttingCase == 0) //e1 & e2, first intersection at e1
        {
            // vertex and normals
            
            // v0, I0, I1
                       
            // I0, V1, V2
            
            // I1, I0, V2
            
            // first triangle
            newTriangles.Add(triangleIndex);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
            
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex]]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 2]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 1]);
            
            // second triangle
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
            newTriangles.Add(triangleIndex + 2);

            triangleVectorList.Add(newVertices[newVertexIndices.Count - 1]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 2]);
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex+2]]);
            
            // third triangle
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
            newTriangles.Add(triangleIndex + 1);
            newTriangles.Add(triangleIndex + 2);
            
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 2]);
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex+1]]);
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex+2]]);
  
        }
        else if (cuttingCase == 1) // e1 & e3, first intersection at e1
        {
            // V0, I0, I1
            
            // I0, V1, I1
            
            // V0, I1, V2
            
            
            
            // first triangle
            newTriangles.Add(triangleIndex);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
            
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex]]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 2]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 1]);
            
            // second triangle
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
            newTriangles.Add(triangleIndex+1);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
            
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 2]);
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex+1]]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 1]);
            
            // third triangle
            newTriangles.Add(triangleIndex);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
            newTriangles.Add(triangleIndex+2);
            
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex]]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 1]);
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex+2]]);
        }
        else if (cuttingCase == 2) // e1 & e3, first intersection at e2
        {
            
            // V0, V1, I2
            
            // V0, I1, I0
            
            // I0, I1, V2
            
            
            // first triangle
            newTriangles.Add(triangleIndex);
            newTriangles.Add(triangleIndex+1);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
            
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex]]);
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex+1]]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 1]);
            
            // second triangle
            newTriangles.Add(triangleIndex);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
            
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex]]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 1]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 2]);
            
            // third triangle
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 2]);
            newTriangles.Add(newVertexIndices[newVertexIndices.Count - 1]);
            newTriangles.Add(triangleIndex+2);
            
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 2]);
            triangleVectorList.Add(newVertices[newVertexIndices.Count - 1]);
            triangleVectorList.Add(electroSnareVR.canvasMesh.vertices[electroSnareVR.canvasMesh.triangles[triangleIndex+2]]);
        }
    }
    
    void InitNewGameObjects()
    {
        cancerCellsObject = new GameObject("cancerCells");
        MeshFilter cancerMeshFilter = cancerCellsObject.AddComponent<MeshFilter>();
        cancerMeshFilter.mesh = new Mesh();

        cervixObject = new GameObject("cervixObject");
        MeshFilter cervicMeshFilter = cervixObject.AddComponent<MeshFilter>();
        cervicMeshFilter.mesh = new Mesh();
    }
    
    /*
    void SeperateMesh
    (
        List<Vector3> cancerVertices, 
        List<Vector3> cervixVertices, 
        List<int> vertexIndicesCancerMesh, 
        List<int> vertexIndicesCervixMesh, 
        List<Vector3> cancerNormals, 
        List<Vector3> cervixNormals,
        List<Vector2> cancerUVs,
        List<Vector2> cervicUVs,
        List<int> cancerTriangles,
        List<int> cervixTriangles
    )
    {
        // Ensure the RenderTexture is active
        RenderTexture.active = electroSnareVR.textureToColor;

        // Create a new Texture2D to store the pixels
        Texture2D texture = new Texture2D(2048, 2048);

        // Read pixels from the RenderTexture into the Texture2D
        texture.ReadPixels(new Rect(0, 0, 2048, 2048), 0, 0);
        texture.Apply(); // Apply changes to the Texture2D

        // Reset the active RenderTexture
        RenderTexture.active = null;
        
        // store the indices of the original order in the two lists.. necessary for later accessing the correct indices
        int i = 0;

        
        // here all new mesh info is inserted. Store hull edge vertices in a list and add it to both meshes.
        // calc triangles as usual, if all corresponding vertices are present. Maybe check with list.Contains?
        
        // Now, 'texture' contains the pixel data from the RenderTexture
        foreach (var uv in electroSnareVR.canvasMesh.uv)
        {
            Color pixelColor = texture.GetPixelBilinear(uv.x, uv.y);
            if (pixelColor == Color.red)
            {
                // add corresponding vertex to the list
                cancerVertices.Add(electroSnareVR.canvasMesh.vertices[i]);
                cancerNormals.Add(electroSnareVR.canvasMesh.normals[i]);
                cancerUVs.Add(electroSnareVR.canvasMesh.uv[i]);
                vertexIndicesCancerMesh.Add(i);
            }
            else
            {
                //TODO: add the other vertices to the other list for the cervic mesh
                cervixVertices.Add(electroSnareVR.canvasMesh.vertices[i]);
                cervixNormals.Add(electroSnareVR.canvasMesh.normals[i]);
                cervicUVs.Add(electroSnareVR.canvasMesh.uv[i]);
                vertexIndicesCervixMesh.Add(i);
            }
            i++;
        }

        foreach (int triangleIndex in electroSnareVR.canvasMesh.triangles)
        {
            int vertexIndex1 = electroSnareVR.canvasMesh.triangles[triangleIndex * 3];
            int vertexIndex2 = electroSnareVR.canvasMesh.triangles[triangleIndex * 3 + 1];
            int vertexIndex3 = electroSnareVR.canvasMesh.triangles[triangleIndex * 3 + 2];

            // Check if all three vertices of the triangle belong to the cancer or cervix mesh
            if (vertexIndicesCancerMesh.Contains(vertexIndex1) &&
                vertexIndicesCancerMesh.Contains(vertexIndex2) &&
                vertexIndicesCancerMesh.Contains(vertexIndex3))
            {
                // This triangle belongs to the cancer mesh
                cancerTriangles.Add(vertexIndicesCancerMesh.IndexOf(vertexIndex1));
                cancerTriangles.Add(vertexIndicesCancerMesh.IndexOf(vertexIndex2));
                cancerTriangles.Add(vertexIndicesCancerMesh.IndexOf(vertexIndex3));
            }
            else if (vertexIndicesCervixMesh.Contains(vertexIndex1) &&
                     vertexIndicesCervixMesh.Contains(vertexIndex2) &&
                     vertexIndicesCervixMesh.Contains(vertexIndex3))
            {
                // This triangle belongs to the cervix mesh
                cervixTriangles.Add(vertexIndicesCervixMesh.IndexOf(vertexIndex1));
                cervixTriangles.Add(vertexIndicesCervixMesh.IndexOf(vertexIndex2));
                cervixTriangles.Add(vertexIndicesCervixMesh.IndexOf(vertexIndex3));
            }
        }
        
    }
*/

    /*
    void FrankensteinMesh
    (
    List<Vector3> cancerVertices,
    List<Vector3> cancerNormals,
    List<Vector2> cancerUVs,
    List<int> cancerTriangles,
    List<Vector3> newMeshVertices, 
    List<int> newVertexIndices,
    List<Vector3> newNormals, 
    List<Vector2> newUVCoordinates, 
    List<int> newTriangles,
    List<Vector3> newTrianglesVertices,
    
    List<Vector2> hull,
    List<Vector3> hullTriangleList,
    List<Vector3> hullVertices3DList,
    List<Vector3> hullVerticesNormalList
    )
    {
        // hull vertices and triangles. TODO: Would have to be added to both meshes

        
        Debug.Log("hullSize: " + hull.Count);
        Debug.Log("hullVertices: " + hullVertices3DList.Count);
        Debug.Log("Hull normals: " + hullVerticesNormalList.Count);
        
        // size of hull
        int hullSize = hull.Count;
        
        // add new hull vertices3D
        cancerVertices.AddRange(hullVertices3DList);
        
        // add normals
        cancerNormals.AddRange(hullVerticesNormalList);
        
        // add uv
        // convert hull to uvs
        List<Vector2> hullUVs = new List<Vector2>();
        foreach (var h in hull)
        {
            Vector2 hullUV = new Vector2();
            hullUV.x = h.x / (2048 - 1);
            hullUV.y = h.y / (2048 - 1);
            hullUVs.Add(hullUV);
        }
        cancerUVs.AddRange(hullUVs);
        
        
        
        // intersection vertices and triangles
        
        
        // get size of existing vertex list
        int vertexSize = cancerVertices.Count;
 
        
        // add up the indices to correctly match the indices for the new mesh
        newVertexIndices = newVertexIndices.Select(x => x + (hullSize)).ToList();
        //newVertexIndices = newVertexIndices.Select(x => x + (vertexSize+hullSize)).ToList();
        
        // here comes the hard part: add the triangles
        // how: iterate through triangles, check if all three vertices are present in the vertexList. If so, get 
        // their indices. 
        // Problem: the indices of the triangels point to the newly created newVertices and from the original mesh
        // for the already existing vertices. 
        // Solution: create map of those indices and vector3. Then check with contain if all are in the vertices 
        // and get their index?
        FrankensteinTriangles(cancerVertices, cancerTriangles, newTrianglesVertices);
        
        //FrankensteinTriangles(cancerVertices, cancerTriangles, hullTriangleList);
    }

    /// <summary>
    /// cancerVertices includes all Vertices, thus also the new ones
    /// cancerTriangles is the index list of the old triangles, only including those in the cancerMe4sh
    /// newTrianglesVertices has the new planned triangles as 3d vectors
    /// </summary>
    /// <param name="cancerVertices"></param>
    /// <param name="cancerTriangles"></param>
    /// <param name="newTrianglesVertices"></param>
    void FrankensteinTriangles(List<Vector3> cancerVertices,List<int> cancerTriangles, List<Vector3> newTrianglesVertices)
    {
        // iterate through the newTrianglesVertices list with a stepsize of 3. Check if the mesh already has these
        // vertices. If so, add the their indices to the triangle list
        for (int i = 0; i < newTrianglesVertices.Count; i+=3)
        {
            var v1 = newTrianglesVertices[i];
            var v2 = newTrianglesVertices[i+1];
            var v3 = newTrianglesVertices[i+2];

            // Find the indices of the vertices in the mesh.vertices array.
            int index1 = cancerVertices.FindIndex(v => v == v1);
            int index2 = cancerVertices.FindIndex(v => v == v2);
            int index3 = cancerVertices.FindIndex(v => v == v3);

            // mesh contains all three vertices
            if (index1 >= 0 && index2 >= 0 && index3 >= 0)
            {
                // add their indices to the mesh.triangles list
                cancerTriangles.Add(index1);
                cancerTriangles.Add(index2);
                cancerTriangles.Add(index3);
            }
        }
    }
    */
    
    /*
    /// <summary>
    /// 
    /// </summary>
    /// <param name="cancerVertices"></param>
    /// <param name="cervixVertices"></param>
    /// <param name="vertexIndicesCancerMesh"></param>
    /// <param name="vertexIndicesCervixMesh"></param>
    /// <param name="newMeshVertices"></param>
    /// <param name="newVertexIndices"></param>
    /// <param name="newNormals"></param>
    /// <param name="newUVCoordinates"></param>
    /// <param name="newTriangles"></param>
    void GenerateMeshes
    (
        List<Vector3> cancerVertices, 
        List<Vector3> cervixVertices, 
        List<int> vertexIndicesCancerMesh, 
        List<int> vertexIndicesCervixMesh, 
        List<Vector3> newMeshVertices, 
        List<int> newVertexIndices, 
        List<Vector3> newNormals, 
        List<Vector2> newUVCoordinates, 
        List<int> newTriangles
    )
    {
        // create new triangle list out of old one
        List<int> triangleCopy = electroSnareVR.canvasMesh.triangles.ToList();

        List<int> cancerTriangles = new List<int>();
        List<int> cervicTriangles = new List<int>();

        List<Vector2> cancerUVCoordinates = new List<Vector2>();
        List<Vector2> cervixUVCoordinates = new List<Vector2>();

        List<Vector3> cancerNormals = new List<Vector3>();
        List<Vector3> cervixNormals = new List<Vector3>();

        // iterate through all already existing triangles from the cervixMesh
        for (int triangleIndex = 0; triangleIndex < triangleCopy.Count; triangleIndex += 3)
        {
            // get vertex indices from one triangle
            int vertexIndex1 = triangleCopy[triangleIndex];
            int vertexIndex2 = triangleCopy[triangleIndex+1];
            int vertexIndex3 = triangleCopy[triangleIndex+2];
            
            // check if all three vertex indices are inside the cancerMesh
            int comparisonCounter = 0;
            foreach (var cancerVertexIndice in vertexIndicesCancerMesh)
            {
                if (cancerVertexIndice == vertexIndex1 || cancerVertexIndice == vertexIndex2 ||
                    cancerVertexIndice == vertexIndex3)
                    comparisonCounter++;
            }

            if (comparisonCounter == 3)
            {
                cancerTriangles.Add(triangleCopy[triangleIndex]);
                cancerTriangles.Add(triangleCopy[triangleIndex+1]);
                cancerTriangles.Add(triangleCopy[triangleIndex+2]);
            }
            else
            {
                cervicTriangles.Add(triangleCopy[triangleIndex]);
                cervicTriangles.Add(triangleCopy[triangleIndex+1]);
                cervicTriangles.Add(triangleCopy[triangleIndex+2]);
            }
        }

        Debug.Log("Number of triangle indices in new cancerTriangles: " + cancerTriangles.Count);
        Debug.Log("Number of triangle indices in new cervixTriangles: " + cancerTriangles.Count);

        Debug.Log("Size of vertices : " + cancerVertices.Count.ToString());
        Debug.Log("Size of indices:" + vertexIndicesCancerMesh.Count.ToString());
        
        // add newMeshVertices to both meshes, as well as their uv coordinates and normals
        // add the triangles to the objects, but only those with all existing vertices



    }
    */
    
    void FindNewIntersectingVertexPositionsV2(Vector2 ts1, Vector2 te1, Vector2 ts2, Vector2 te2, Vector2 ts3, Vector2 te3, Vector2 edgeS, Vector2 edgeE, out Vector2 inP1, out Vector2 inP2, out Vector2 inP3)
    {
        // return Vector2.zero if no intersection point found
        inP1 = CalcIntersectionPoint(ts1, te1, edgeS, edgeE);

        inP2 = CalcIntersectionPoint(ts2, te2, edgeS, edgeE);

        inP3 = CalcIntersectionPoint(ts3, te3, edgeS, edgeE);
    }
    
    
    /// <summary>
    /// Checks the intersection points between three triangle lines and one cutting line
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="v1"></param>
    /// <param name="v2"></param>
    /// <param name="v3"></param>
    /// <returns></returns>
    List<Vector2> FindNewIntersectingVertexPositions(Vector2 ts1, Vector2 te1, Vector2 ts2, Vector2 te2, Vector2 ts3, Vector2 te3, Vector2 edgeS, Vector2 edgeE, out int cuttingCase)
    {
        List<Vector2> intersections = new List<Vector2>();
        
        Vector2 inP1 = CalcIntersectionPoint(ts1, te1, edgeS, edgeE);

        Vector2 inP2 = CalcIntersectionPoint(ts2, te2, edgeS, edgeE);

        Vector2 inP3 = CalcIntersectionPoint(ts3, te3, edgeS, edgeE);

        bool intersectionAtE12 = false;
        bool intersectionAtE13 = false;
        bool intersectionAtE23 = false;

        if (inP1 != Vector2.zero)
        {
            intersectionAtE12 = true;
            intersections.Add(inP1);
        }

        if (inP2 != Vector2.zero)
        {
            intersectionAtE13 = true;
            intersections.Add(inP2);
        }

        if (inP3 != Vector2.zero)
        {
            intersectionAtE23 = true;
            intersections.Add(inP3);
        }

        // three cases:
        // e1 & e2, e1 & e3, e2 & e3
        if (intersectionAtE12 && intersectionAtE13)
            cuttingCase = 0;
        else if (intersectionAtE12 & intersectionAtE23)
            cuttingCase = 1;
        else if (intersectionAtE13 && intersectionAtE23)
            cuttingCase = 2;
        else
            cuttingCase = -1;
        
        return intersections;
    }
    
    /// <summary>
    /// calculates the intersection point between two lines in 2D
    /// </summary>
    /// <param name="s1"></param>
    /// <param name="e1"></param>
    /// <param name="s2"></param>
    /// <param name="e2"></param>
    /// <returns></returns>
    Vector2 CalcIntersectionPoint(Vector2 s1, Vector2 e1, Vector2 s2, Vector2 e2)
    {
        // direction vectors
        Vector2 dir1 = e1 - s1;
        Vector2 dir2 = e2 - s2;

        // determinante
        float det = dir1.x * dir2.y - dir1.y * dir2.x;

        // if lines are parallel (0)
        if (Mathf.Approximately(det, 0f))
        {
            // no intersection
            return Vector2.zero;
        }

        // Calculate t values for each line segment
        float t1 = ((s2.x - s1.x) * dir2.y - (s2.y - s1.y) * dir2.x) / det;
        float t2 = ((s2.x - s1.x) * dir1.y - (s2.y - s1.y) * dir1.x) / det;

        // Check if the intersection point is within both line segments
        if (t1 >= 0f && t1 <= 1f && t2 >= 0f && t2 <= 1f)
        {
            // Calculate and return the intersection point
            Vector2 intersection = s1 + t1 * dir1;
            return intersection;
        }

        // no intersection
        return Vector2.zero;
    }
    
    /// <summary>
    /// Creating the cutting area consisting of the sampled hit and transform position
    /// </summary>
    void createCuttingArea()
    {
        // newVertices includes all vertices
        // project the vertices into a 2d plane, use delauny triangulation to create the triangle information
        // project back to 3d mesh
        // add vertices and triangle information to the 2 created meshes....?

        // create empty list for projected vertices
        List<Vector2> projectionList = new List<Vector2>();
        
        // create projection matrix, rotating the y axis 30 degree
        /*
         | cos(30)  0  sin(30) |
        |   0     1    0     |
        | -sin(30) 0  cos(30) |
         */
        Matrix4x4 rotationMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 30, 0));
        
        // project vertices onto a 30 degree plane, ignoring the z axis
        foreach (var vertex in newVertices)
        {
            Vector2 projectedVertex = new Vector2(rotationMatrix.MultiplyPoint(vertex).x,
                rotationMatrix.MultiplyPoint(vertex).y);
            
            projectionList.Add(projectedVertex);
        }
    }
    
    
      
    void DebugVertices(List<Vector3> newVertices, Color colour)
    {
        float sphereScale = 0.0001f;
        
        // Create a sphere mesh for the small spheres
        GameObject spherePrefab = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(spherePrefab.GetComponent<Collider>()); // Remove the collider
            
        // Set the sphere prefab to inactive
        //spherePrefab.SetActive(false);
        
        // Apply the blue material to the sphere
        Renderer sphereRenderer = spherePrefab.GetComponent<Renderer>();
        
        Material newM = new Material(Shader.Find("Standard"));
        newM.color = colour;
       
        sphereRenderer.material = newM;
        
        foreach (Vector3 vertexPosition in newVertices)
        {
            // Instantiate a small sphere at the vertex position
            GameObject sphere = Instantiate(spherePrefab); //Instantiate(spherePrefab, electroSnareVR.cervixTransform.TransformPoint(vertexPosition), Quaternion.identity);

            sphere.transform.position = electroSnareVR.cervixTransform.TransformPoint(vertexPosition);
            
            // Set the scale of the sphere
            sphere.transform.localScale = new Vector3(sphereScale, sphereScale, sphereScale);
            

            //sphere.transform.localScale = new Vector3(sphereScale, sphereScale, sphereScale);

        }
    }


    void DebugTriangles(List<Vector3> triangles)
    {
        float sphereScale = 0.0001f;
        
        // Create a sphere mesh for the small spheres
        GameObject spherePrefab = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(spherePrefab.GetComponent<Collider>()); // Remove the collider
            
        // Set the sphere prefab to inactive
        //spherePrefab.SetActive(false);
        
        // Apply the blue material to the sphere
        Renderer sphereRenderer = spherePrefab.GetComponent<Renderer>();
        
        Material newM = new Material(Shader.Find("Standard"));
        newM.color = Color.magenta;
       
        sphereRenderer.material = newM;
        
        foreach (Vector3 vertexPosition in triangles)
        {
            // Instantiate a small sphere at the vertex position
            GameObject sphere = Instantiate(spherePrefab, electroSnareVR.cervixTransform.TransformPoint(vertexPosition), Quaternion.identity);
            
            sphere.transform.localScale = new Vector3(sphereScale, sphereScale, sphereScale);

        }
    }
    
}
