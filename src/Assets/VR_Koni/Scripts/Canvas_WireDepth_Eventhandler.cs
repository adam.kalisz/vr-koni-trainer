using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Canvas_WireDepth_Eventhandler : MonoBehaviour
{


    private Transform mCanvas;

    private Transform mText;
    private TextMeshPro mTextComponent;

    //private TextMeshPro mText;
    
    // Start is called before the first frame update
    void Start()
    {
        mCanvas = transform.Find("Canvas");//transform.Find("Canvas");
        
        mText = mCanvas.GetChild(0);
        mTextComponent = mText.GetComponent<TextMeshPro>();
        //mText = mCanvas.GetComponent<TextMeshPro>();

        ElectroSnareVR.onCuttingLengthChange += UpdateText;
        //Timer.onTimeChange += UpdateText;
    }

    private void OnDisable()
    {
        ElectroSnareVR.onCuttingLengthChange -= UpdateText;
        //Timer.onTimeChange += UpdateText;
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateText(float wireDepth)
    {
        wireDepth *= 100;
        mTextComponent.SetText(wireDepth.ToString());
    }

}
