using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Canvas_WireState_Eventhandler : MonoBehaviour
{
    private Transform mCanvas;

    private Transform mText;
    private TextMeshPro mTextComponent;
    
    // Start is called before the first frame update
    void Start()
    {
        mCanvas = transform.Find("Canvas");//transform.Find("Canvas");
        
        mText = mCanvas.GetChild(0);
        mTextComponent = mText.GetComponent<TextMeshPro>();

        ElectroSnareVR.onWireStateChange += UpdateText;
    }

    private void OnDestroy()
    {
        ElectroSnareVR.onWireStateChange -= UpdateText;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateText(bool isWireActive)
    {
        string textToDisplay = "";
        switch (isWireActive)
        {
            case true:
                textToDisplay = "Hot";
                mTextComponent.color = Color.red;
                break;
            case false:
                textToDisplay = "Cold";
                mTextComponent.color = Color.blue;
                break;
        }
        
        mTextComponent.SetText(textToDisplay);
    }
}
