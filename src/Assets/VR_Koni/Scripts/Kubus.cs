using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

/// <summary>
/// This script is only used to store the original texture at the beginning of the game and restore it with an
/// backup at the end of the game
/// </summary>
public class Kubus : MonoBehaviour
{
    private Texture2D mTexture;
    private Texture2D mCanvas;
    
    // backup texture data
    private Color[] originalPixels;
    
    public int textureWidth = 2048;
    public int textureHeight = 2048;
    public RenderTextureFormat textureFormat = RenderTextureFormat.ARGB32;//RenderTextureFormat.RGB565; //RenderTextureFormat.ARGB32;
    private RenderTexture renderTexture;
    
    // Start is called before the first frame update
    void Start()
    {
        Renderer rend = GetComponent<Renderer>();
        if (rend != null && rend.sharedMaterial != null)
        {
            mTexture = rend.sharedMaterial.GetTexture("_MainTex") as Texture2D;

            renderTexture = new RenderTexture(textureWidth, textureHeight, 0, textureFormat, RenderTextureReadWrite.Linear);
            renderTexture.enableRandomWrite = true; // Enable the UAV flag
            
            rend.sharedMaterial.SetTexture("_SecondTex", renderTexture);
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDisable()
    {
        renderTexture.Release();
    }
}
