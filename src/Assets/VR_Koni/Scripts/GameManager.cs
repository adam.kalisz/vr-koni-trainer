using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    SelectScreen,
    Operation,
    Victory,
    Loose
}

/// <summary>
/// GameManger is a singleton
/// </summary>
public class GameManager : MonoBehaviour
{

    // Singleton GameManager
    public static GameManager Instance;

    public GameState State;

    public static event Action<GameState> OnGameStateChanged;
    
    private void Awake()
    {
        Instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        // TODO: Change to Select Screen when further in development
        UpdateGameState(GameState.Operation);
        
        ElectroSnareVR.onChangingState += ReceiveCuttingCondition;

        Timer.onTimeChange += ReceiveCuttingTime;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDisable()
    {
        ElectroSnareVR.onChangingState += ReceiveCuttingCondition;

        Timer.onTimeChange -= ReceiveCuttingTime;
    }
    
    public void UpdateGameState(GameState newState)
    {
        State = newState;

        switch (newState)
        {
            case GameState.SelectScreen:
                break;
            case GameState.Operation:
                break;
            case GameState.Victory:
                break;
            case GameState.Loose:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }
        
        OnGameStateChanged?.Invoke(newState);
    }

    // TODO: extend functionality or conditions for loosing state
    /// <summary>
    /// Touching the speculum or vagina with the activated (i.e., energized) wire loop.
    /// </summary>
    /// <param name="cuttingState"></param>
    void ReceiveCuttingCondition(ElectroSnareVR.CuttingState cuttingState)
    {
        if (cuttingState == ElectroSnareVR.CuttingState.CancerTissue)
        {
            UpdateGameState(GameState.Loose);
        }
    }

    /// <summary>
    /// Activated wire loop remains in the tissue of the cervix for longer than 8 seconds
    /// (this way we want to avoid that participants guide the wire loop too slowly and thus cause the beforementioned problem).
    /// </summary>
    /// <param name="time"></param>
    void ReceiveCuttingTime(float time)
    {
        if (time > 8.0f)
        {
            UpdateGameState(GameState.Loose);
        }
    }
}
