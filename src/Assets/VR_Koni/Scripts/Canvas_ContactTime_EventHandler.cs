using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Canvas_ContactTime_EventHandler : MonoBehaviour
{
    private Transform mCanvas;

    private Transform mText;
    private TextMeshPro mTextComponent;

    //private TextMeshPro mText;
    
    // Start is called before the first frame update
    void Start()
    {
        mCanvas = transform.Find("Canvas");//transform.Find("Canvas");
        
        mText = mCanvas.GetChild(0);
        mTextComponent = mText.GetComponent<TextMeshPro>();
        
        Timer.onTimeChange += UpdateText;
        //Elektroschlinge_VR.onCuttingLengthChange += UpdateText;
       
    }

    private void OnDisable()
    {
        Timer.onTimeChange -= UpdateText;
        //Elektroschlinge_VR.onCuttingLengthChange -= UpdateText;
        
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateText(float time)
    {
        mTextComponent.SetText(time.ToString());
    }
}
