
using System.Collections.Generic;
using UnityEngine;
using System;
using Color = UnityEngine.Color;

using Valve.VR;
using Valve.VR.InteractionSystem;


public class ElectroSnareVR : MonoBehaviour
{

    // ===== Events =====
    
    public static event Action<float> onCuttingLengthChange;

    public static event Action<CuttingState> onChangingState;

    public static event Action<CuttingState> onFirstTimeCutChanges; 
    
    public static event Action<double> onVolumeChange;

    public static event Action<bool> onWireStateChange; 
    
    
    // event for cutting
    public static event Action<bool> onRayCastFire; 

    // ===== ===== =====

    
    
    // ===== SteamVR Input Actions =====
    
    public SteamVR_Action_Boolean actionWireBoolean = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("elektroschlinge", "ActivateWire");
    
    // ===== ===== =====
    
    
    
    // ===== Serialized Fields =====
    
    [Header("Name of Koni Object")]
    [Tooltip("Use this to decide wich object the Elektroschlinge should interact with")]
    [SerializeField]
    public String ObjectToHit = "koniUVEdited";
    
    // const variables to differentiate the colours.
    [Header("Cancer Tissue Threshold Color")]
    [Tooltip("Use this to adjust the color for the colored cancer tissue")]
    [SerializeField]
    public Color CancerTissueThresholdColor = new Color(0.95f, 0.95f, 0.95f); 
    
    // this color is utilized as a default color to use the statemachine with
    [Header("Default no Hit Color")]
    [Tooltip("Use this to adjust the color if nothing is hit.")]
    [SerializeField]
    public Color DefaultNoHitColor = Color.black;

    [Header("Cutting Color")] 
    [Tooltip("Use this to adjust the color for the cuts")]
    [SerializeField]
    public Color cuttingColor = Color.green;
    
    //TODO: make const and not serialized
    [Header("Brush Size")] 
    [Tooltip("Use this to adjust the Brush size for the cut drawing")]
    [SerializeField]
    public int brushSize = 1;
    
    // Parabola function: -1.5x^2 + 0x + 2
    [Header("Approximate Parabola of Wire")]
    public static readonly Polynomial WireParabola = new Polynomial(new List<float> { -1.5f, 0f, 2.0f });
    
    // Integral 0.1x^3 + 0x^2 -9x + 0
    // currently not used
    //public static readonly Polynom WireParabolaIntegral = new Polynom(new List<float>{0.1f, 0f, 9f, 0f});
    
    // ===== ===== =====
    
    // Define the color thresholds for Area1 and Area2
    public Color cancerTissue = new Color(240 / 255f, 180 / 255f, 230 / 255f);
    public Color healthyTissue = new Color(200 / 255f, 130 / 255f, 180 / 255f);
    
    // ===== References to Components =====
    
    // ref to own rigidbody
    private Rigidbody mRigidbody;
    
    // ref to raycast position for wire depth detection
    private Transform mainRayCast;
    
    // ref to left and right raycast positions
    private Transform leftRayCast;
    private Transform rightRayCast;
    
    // ref to lower left and righ raycast positions
    private Transform leftRayCast_1;
    private Transform rightRayCast_1;
    private Transform leftRayCast_2;
    private Transform rightRayCast_2;
    private Transform leftRayCast_3;
    private Transform rightRayCast_3;
    
    // ref to wire top position
    private Transform wireTop;
    
    // arrays to hold all raycasts. used to iterate through all raycasts for each side
    public List<Transform> leftRayCastList;
    public List<Transform> rightRayCastList;

    // ref to interactable script component (SteamVR origin)
    private Interactable interactable;

    
    
    // ===== ===== =====
    
    
    // ===== enums and struts =====
    
    // Enum for cutting states
    public enum CuttingState
    {
        NonCutting,
        HealthyTissue,
        CancerTissue
    };
    
    // struct to store CutInfo
    public struct CutInfo
    {
        public float wireDepth;
        public Vector3 wireTip;

        public CutInfo(float wireDepth, Vector3 wireTip)
        {
            this.wireDepth = wireDepth;
            this.wireTip = wireTip;
        }
        
        // set default values
        public static CutInfo Default => new CutInfo(0.0f, new Vector3(0.0f,0.0f,0.0f));
    }

    // struct to store RayCastPair
    struct RayCastPair
    {
        public RaycastHit _leftHit;
        public RaycastHit _rightHit;

        private RayCastPair(RaycastHit left, RaycastHit right)
        {
            _leftHit = left;
            _rightHit = right;
        }
        
        public static RayCastPair Default => new RayCastPair(default, default);
    }
    
    // struct to store Polynoms
    public struct Polynomial
    {
        public List<float> coefficients;
        public float power;

        public Polynomial(List<float> coefficientsArgs)
        {
            coefficients = new List<float>(coefficientsArgs);
            power = coefficients.Count;
        }

        public float GetCoefficient(int index)
        {
            return coefficients[index];
        }
    }
    
    // ===== ===== =====
    
    
    
    // ===== private Variables =====
    
    // CuttingDepth(LengthOfWire)
    private const float raylength = 0.0186f;
    
    // minimumCutLength
    //private const float minCutLength = 0.01f;

    // variable to store current wireDepth
    private float wireDepth;
    
    // bool to manage if the wire is activate or not.
    // Could be made into state machine
    private bool previousWireState = false;
    private bool isWireActive = false;
    
    // Value to keep track of the current cutting state
    // previous state to keep track of the change
    // Could be made into state machine
    private CuttingState cuttingState = CuttingState.NonCutting;
    private CuttingState previousCuttingState = CuttingState.NonCutting;
    
    // struct to store a raycast pair. used to determine where the cutting line between the left and right raycast should be
    private RayCastPair rayPair;

    // variable to store Texture to color - should be the texture of the Koni Object
    //private Texture2D textureToColor = null;
    
    // TODO: public is not pretty, should be private. But other script nees it and access it again is more performant
    public RenderTexture textureToColor = null;
    private Texture2D textureToRead = null;

    // 
    public Material canvasMaterial;
    public Transform cervixTransform;
    
    public Mesh canvasMesh;
    
    public MeshRenderer canvasMeshRenderer;

    private Renderer koniRenderer;

    public ComputeShader computeShader;
    
    private int kernelID;

    public List<Vector2> allDrawnPixel;
    public List<RaycastHit> allLeftHits;
    public List<RaycastHit> allRightHits;
    public Dictionary<Vector2, RaycastHit> hullRaycastHits;
    

    // structs and bool to store position Vectors for area calculations
    private bool storeFirstPosPair = true;
    private CutInfo cutInfo1;
    private CutInfo cutInfo2;
    
    
    private double completeVolume = 0;  // this is used
    
    //private RayCastPair firstVolumeCut;
    //private RayCastPair secondVolumeCut;

    // list to store wiretop positions
    //private List<float> wireDepthList = new List<float>();
    
    // ===== ===== =====
    
    
    // Needed for cutting
    public int rightHitIndex = -1;
    public int leftHitIndex = -1;

    public RaycastHit currentLeftHit;
    public RaycastHit currentRightHit;
    

    

    // ===== Unity Methods =====

    private void Awake()
    {
        allDrawnPixel = new List<Vector2>();
        allLeftHits = new List<RaycastHit>();
        allRightHits = new List<RaycastHit>();
        hullRaycastHits = new Dictionary<Vector2, RaycastHit>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Fetch the Rigidbody from the GameObject with this script attached
        mRigidbody = GetComponent<Rigidbody>();
        
        // Find raycast origin component to use its position later
        mainRayCast = transform.Find("RayCastOrigin");

        // init raycasts and fill lists
        InitRayCasts();
        
        // Fetch Interactable Component. Used to manage button inputs
        interactable = GetComponent<Interactable>();

        kernelID = computeShader.FindKernel("CSMain");
    }

    // Update is called once per frame
    void Update()
    {
        // call function to manage the wire state. This is in Update because it uses SteamVR input system
        ManageWireState();
        
        MainActionLoop();
    }
    
    // Executed at a specific rate defined in the editor. Dependent on the Physics Manager
    // WARNING: DO NOT TRY TO CHANGE THE PHYSICS SPEED! IT WILL LAED TO NOTHING BUT PAIN!!!
    private void FixedUpdate()
    {
       
    }
    

    /// <summary>
    /// OnCollisionEnter calls when this collider collides with another collider.
    /// The setting of the MovePosition and MoveRotation and isKinematic is done to essentially stop the object from
    /// moving when it collides with something.
    /// Otherwise, it would slowly float away
    /// </summary>
    /// <param name="otherCollider"></param>
    private void OnCollisionEnter(Collision otherCollider)
    {
        mRigidbody.MovePosition(mRigidbody.position);
        mRigidbody.MoveRotation(mRigidbody.rotation);
        mRigidbody.isKinematic = true;
    }
    
    /// <summary>
    /// OnCollisionEnter calls when this collider exits the collision with another collider.
    /// The setting of the MovePosition and MoveRotation and isKinematic is done to essentially stop the object from
    /// moving when it collides with something.
    /// Otherwise, it would slowly float away
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionExit(Collision other)
    {   
        mRigidbody.isKinematic = false;
        mRigidbody.MovePosition(mRigidbody.position);
        mRigidbody.MoveRotation(mRigidbody.rotation);
    }

    // ===== ===== =====
    
    
    
    // ===== Main Loops, State Machines =====
    
    /// <summary>
    /// This is the main loop for the whole script.
    /// Essentially, it tries to detect the Koni Object. If this is successful, the wirelength, volume, time and other
    /// values are calculated, the cuts are drawn, state machine changes and events are fired
    /// </summary>
    private void MainActionLoop()
    {
        // only check hits if the wire is active
        if (!isWireActive)
        {
            // set state to the default when no hit is detected
            SetCuttingStateByColorDebugging(DefaultNoHitColor);

            return;
        }
        
        // get raycast hit of middle point of the instrument. 
        RaycastHit hit;
        if (Physics.Raycast(mainRayCast.position, mainRayCast.TransformDirection(Vector3.forward), out hit, raylength))
        {
            // check if hit object is from type kubus
            if (hit.collider.gameObject.name == ObjectToHit)
            {
                // set textureToColor and textureToRead
                if (textureToColor == null || textureToRead == null)
                    SetTextures(hit);
                
                // get wire depth
                wireDepth = CalculateWireDepth(hit);
            
                // invoke events to notify UI/GameManager
                onCuttingLengthChange?.Invoke(wireDepth);
            
                // fire all raycasts and fill the two lists of hits
                RayCastPair rayCastPair = FireRayCasts();

                // define color from the two hits from the side of the wire. only returns healthy when both sides 
                // are in the health zone
                Color color = RecognizeColorHitsDebug(rayCastPair);
                
                // recognizes the color that the raycast is hitting. If no collision occurs, it returns the defaultnohitcolor
                // method to check for debug textures (clear white)
                //Color color = RecognizeColorHit(ref hit);
                // method to check for realistic textures
                //Color color = RecognizeColorHitArea(ref hit, 2);
            
                // setting the state
                SetCuttingStateByColorDebugging(color);
                   
                // Calculate Area. To do this we do the following:
                // Every X Seconds, the points of the middle RayCastHit and the WireTip are Stored in a List
                // When enough points are in the List to from a Trapez, we can calculate the area of the trapez and 
                // multiply it with the area of the wire. To calculate the area of the wire, we take the middle value of the
                // two hits from the mainraycast as m of a straight line and intersect it with the static wire parabole

                if (cuttingState == CuttingState.HealthyTissue)
                {
                    // calc volume between between two "hits" and add it to the sum
                    double tempVolume = CalcCorrectVolume(wireDepth, wireTop);
                    completeVolume += tempVolume;
                    
                    // fire volume change event for UI
                    onVolumeChange?.Invoke(completeVolume);

                    // Draw the cuts on the second drawing texture
                    DrawHits(rayCastPair);
                }
            }
        }
        else
        {
            // set state to the default when no hit is detected
            SetCuttingStateByColorDebugging(DefaultNoHitColor);
            
            wireDepth = 0;
            
            // ivoke events to notify UI/GameManager
            onCuttingLengthChange?.Invoke(wireDepth);
        }
    }
    
    /// <summary>
    /// Method to manage the state of the wire (On or Off) via the SteamVR input System
    /// </summary>
    private void ManageWireState()
    {
        if (interactable.attachedToHand)
        {
            var bWireActivation = false;
            SteamVR_Input_Sources hand = interactable.attachedToHand.handType;
            bWireActivation = actionWireBoolean.GetState(hand);
            isWireActive = bWireActivation;
            
            if (isWireActive != previousWireState)
            {
                // fire event that the wire was activated or not
                onWireStateChange?.Invoke(isWireActive);
            }
            previousWireState = isWireActive;
        }
    }
    
    /// <summary>
    /// STATE MACHINE Cutting State
    /// Set the cutting state by comparing input color to CancerTissueThresholdColor
    /// Debugging: Simple, unrealistic Texture
    /// </summary>
    /// <param name="raycastHitColor"></param>
    /// <returns></returns>
    private void SetCuttingStateByColorDebugging(Color raycastHitColor)
    {
        // set current(new) CuttingState
        if (raycastHitColor == DefaultNoHitColor)
        {
            cuttingState = CuttingState.NonCutting;
        }
        else if (raycastHitColor.r >= CancerTissueThresholdColor.r &&
                 raycastHitColor.g >= CancerTissueThresholdColor.g &&
                 raycastHitColor.b >= CancerTissueThresholdColor.b)
        {
            cuttingState = CuttingState.CancerTissue;
        }
        else if (raycastHitColor == cuttingColor) 
        {
            cuttingState = CuttingState.CancerTissue;
        }
        else
        {
            cuttingState = CuttingState.HealthyTissue;
        }

        onChangingState?.Invoke(cuttingState);
        
        // check the previous state
        ManageStateChange();
    }
    
    /// <summary>
    /// STATE MACHINE Cutting State
    /// Set the cutting state by comparing input color to CancerTissueThresholdColor
    /// uses realistic textures. The thresholds were chosen by taking color samples from cancer tissue and the
    /// healthy tissue
    /// NOTE: creates performance problems
    /// </summary>
    /// <param name="raycastHitColor"></param>
    /// <returns></returns>
    private void SetCuttingStateByColor(Color raycastHitColor)
    {
        // Calculate the color difference between the given color and the thresholds
        var cancerTissueDifference = ColorDifference(raycastHitColor, cancerTissue);
        var healthyTissueDifference = ColorDifference(raycastHitColor, healthyTissue);
        
        // set current(new) CuttingState
        if (raycastHitColor == DefaultNoHitColor)
        {
            cuttingState = CuttingState.NonCutting;
        }
        else if (cancerTissueDifference < healthyTissueDifference)
        {
            cuttingState = CuttingState.CancerTissue;
        }
        else if (raycastHitColor == cuttingColor)
        {
            cuttingState = CuttingState.CancerTissue;
        }
        else
        {
            cuttingState = CuttingState.HealthyTissue;
        }

        onChangingState?.Invoke(cuttingState);
        
        // check the previous state
        ManageStateChange();
       
    }
    
    /// <summary>
    /// Method to check the change for the STATE MACHINE Cutting State
    /// </summary>
    private void ManageStateChange()
    {
        // first time switching to cutting cancer
        if ( previousCuttingState == CuttingState.NonCutting
             && cuttingState == CuttingState.HealthyTissue)
        {
            // send events
            onFirstTimeCutChanges?.Invoke(cuttingState);
        }

        // first time leaving the CancerTissue Cutting State
        if (previousCuttingState == CuttingState.HealthyTissue && cuttingState != CuttingState.HealthyTissue)
        {
            // send event
            onFirstTimeCutChanges?.Invoke(cuttingState);
        }
        
        // set previousCuttingState to current State for next comparison    
        previousCuttingState = cuttingState;
    }
    
    // ===== ===== =====

    
    
    // ===== RayCasts =====

    private void InitRayCasts()
    {
        // Find left and right raycast positions for "line drawing" of the cut
        leftRayCast = transform.Find("RayCastLineLeft");
        rightRayCast = transform.Find("RayCastLineRight");

        // Find additional raycasts
        leftRayCast_1 = transform.Find("RayCastLineLeft_1");
        rightRayCast_1 = transform.Find("RayCastLineRight_1");
        
        leftRayCast_2= transform.Find("RayCastLineLeft_2");
        rightRayCast_2= transform.Find("RayCastLineRight_2");
        
        leftRayCast_3= transform.Find("RayCastLineLeft_3");
        rightRayCast_3= transform.Find("RayCastLineRight_3");
        
        // Find wireTop transform
        wireTop = transform.Find("WireTop");
        
        // init lists
        leftRayCastList = new List<Transform>();
        rightRayCastList = new List<Transform>();
        
        // fill lists
        leftRayCastList.Add(leftRayCast);
        leftRayCastList.Add(leftRayCast_1);
        leftRayCastList.Add(leftRayCast_2);
        leftRayCastList.Add(leftRayCast_3);
        leftRayCastList.Add(wireTop);
        
        rightRayCastList.Add(rightRayCast);
        rightRayCastList.Add(rightRayCast_1);
        rightRayCastList.Add(rightRayCast_2);
        rightRayCastList.Add(rightRayCast_3);
        rightRayCastList.Add(wireTop);
    }

    /// <summary>
    /// small method to set the texture to color. this should be the main texture of the koni object
    /// </summary>
    /// <param name="hit"></param>
    private void SetTextures(RaycastHit hit)
    {
        MeshRenderer rend = hit.collider.GetComponent<MeshRenderer>();
        canvasMeshRenderer = rend;

        canvasMesh = hit.collider.GetComponent<MeshFilter>().mesh;
        canvasMesh.MarkDynamic();
        
        koniRenderer = hit.collider.GetComponent<Renderer>();
        canvasMaterial = rend.material;

        cervixTransform = hit.collider.gameObject.transform;

        //Texture2D tex = rend.sharedMaterial.mainTexture as Texture2D;
        Texture2D tex = rend.sharedMaterial.GetTexture("_MainTex") as Texture2D;
        
        RenderTexture tex2 = rend.sharedMaterial.GetTexture("_SecondTex") as RenderTexture;

        textureToRead = tex;
        textureToColor = tex2;

        // setting the input texture for the compute shader coloring the cut itself
        computeShader.SetTexture(kernelID, "InputTexture", tex2);
    }
    
    
    
    /// <summary>
    /// Method to fire all raycasts from the side of the wire.
    /// Returns one hit for the right side and one hit for the left side.
    /// Why do both sides separately?: If the electro snare is turned, it is not guaranteed that the hit index is the same
    /// </summary>
    private RayCastPair FireRayCasts()
    {
        RayCastPair rayCastPair = new RayCastPair();

        bool rh = false;
        bool lh = false;
        
        // Left raycasts
        for (leftHitIndex = leftRayCastList.Count - 1; leftHitIndex > 0; leftHitIndex--)
        {
            Transform currentRayCast = leftRayCastList[leftHitIndex-1];
            Transform nextRayCast = leftRayCastList[leftHitIndex];

            var currentPos = currentRayCast.position;
            var nextPos = nextRayCast.position;
            
            Vector3 direction = (nextPos - currentPos).normalized;
            float distance = Vector3.Distance(currentPos, nextPos);

            RaycastHit hit;
            if (Physics.Raycast(currentPos, direction, out hit, distance))
            {
                rayCastPair._leftHit = hit;
                
                // TODO: cleaner way to send hit to CuttingAndGeneration
                currentLeftHit = hit;
                lh = true;
            }
        }

        // Right raycasts
        for(rightHitIndex = rightRayCastList.Count -1; rightHitIndex > 0; rightHitIndex--)
        {
            Transform currentRayCast = rightRayCastList[rightHitIndex-1];
            Transform nextRayCast = rightRayCastList[rightHitIndex ];

            var currentPos = currentRayCast.position;
            var nextPos = nextRayCast.position;
            
            Vector3 direction = (nextPos - currentPos).normalized;
            float distance = Vector3.Distance(currentPos, nextPos);

            RaycastHit hit;
            if (Physics.Raycast(currentPos, direction, out hit, distance))
            {
                rayCastPair._rightHit = hit;
                
                // TODO: cleaner way to send hit to CuttingAndGeneration
                currentRightHit = hit;
                rh = true;
            }
        }

        // for cutting
        if (lh && rh) 
            onRayCastFire?.Invoke(true);
        
        return rayCastPair;
    }
    
    // ===== ===== =====

    
    
    // ===== Color Detection, Drawing of Cuts, Pixeloperations =====

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private Color RecognizeColorHitsDebug(RayCastPair raycastPair)
    {
        // test left hit:
        Vector2 leftpixelUV = raycastPair._leftHit.textureCoord;
        Color leftColor = textureToRead.GetPixel(Mathf.FloorToInt(leftpixelUV.x * textureToRead.width), Mathf.FloorToInt(leftpixelUV.y * textureToRead.height));
        
        // test right hit:
        Vector2 rightpixelUV = raycastPair._rightHit.textureCoord;
        Color rightColor = textureToRead.GetPixel(Mathf.FloorToInt(rightpixelUV.x * textureToRead.width), Mathf.FloorToInt(rightpixelUV.y * textureToRead.height));

        // recognise color.
        if (leftColor == rightColor)
            return leftColor;

        if (leftColor == cuttingColor || rightColor == cuttingColor)
            return cuttingColor;

        if ((leftColor.r >= CancerTissueThresholdColor.r) &&
            (leftColor.g >= CancerTissueThresholdColor.g) &&
            (leftColor.b >= CancerTissueThresholdColor.b) ||
            (rightColor.r >= CancerTissueThresholdColor.r) &&
            (rightColor.g >= CancerTissueThresholdColor.g) &&
            (rightColor.b >= CancerTissueThresholdColor.b)
           )
        {
            return CancerTissueThresholdColor;
        }
        
        if (leftColor == DefaultNoHitColor || rightColor == DefaultNoHitColor)
            return DefaultNoHitColor;
        
        return DefaultNoHitColor;
    }

    
    
    
    
    
    /// <summary>
    /// returns color based on texel color of raycasthit
    /// </summary>
    /// <param name="hit"></param>
    /// <returns></returns>
    private Color RecognizeColorHit(ref RaycastHit hit)
    {
        MeshCollider meshCollider = hit.collider as MeshCollider;
        Renderer rend = hit.collider.GetComponent<MeshRenderer>();
            
        if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
            return DefaultNoHitColor;
            
        Texture2D tex = rend.sharedMaterial.mainTexture as Texture2D;
        Vector2 pixelUV = hit.textureCoord;
            
        // tiling and color
        //Vector2 tiling = rend.sharedMaterial.mainTextureScale;
        
        Color color = tex.GetPixel(Mathf.FloorToInt(pixelUV.x * tex.width), Mathf.FloorToInt(pixelUV.y * tex.height));
        
        return color;
    }
    
    /// <summary>
    /// This Method estimates the average color around a RayCast Hit. This is used to estimate if the hit is
    /// in the cancer are or the healthy tissue.
    /// This creates Perfomance Problems and is currently not used
    /// </summary>
    /// <param name="hit"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    private Color RecognizeColorHitArea(ref RaycastHit hit, float radius)
    {
        MeshCollider meshCollider = hit.collider as MeshCollider;
        Renderer rend = hit.collider.GetComponent<MeshRenderer>();
            
        if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
            return DefaultNoHitColor;
            
        Texture2D tex = rend.sharedMaterial.mainTexture as Texture2D;
        Vector2 pixelUV = hit.textureCoord;
            
        // Calculate the texture coordinates within the radius
        float texCoordX = pixelUV.x * tex.width;
        float texCoordY = pixelUV.y * tex.height;
        
        // Calculate the min and max bounds for sampling within the radius
        int startX = Mathf.FloorToInt(texCoordX - radius);
        int endX = Mathf.CeilToInt(texCoordX + radius);
        int startY = Mathf.FloorToInt(texCoordY - radius);
        int endY = Mathf.CeilToInt(texCoordY + radius);

        // Get all pixels within the sample area
        Color32[] colors = tex.GetPixels32();

        // Accumulate the color samples
        Color accumulatedColor = Color.black;
        int sampleCount = 0;

        // Iterate through the sampled colors and average them
        for (int y = startY; y < endY; y++)
        {
            for (int x = startX; x < endX; x++)
            {
                // Calculate the index within the colors array
                int index = y * tex.width + x;

                // Check if the index is within the array bounds
                if (index >= 0 && index < colors.Length)
                {
                    Color32 sampleColor = colors[index];
                    accumulatedColor += sampleColor;
                    sampleCount++;
                }
            }
        }

        // Calculate the average color
        Color averageColor = accumulatedColor / sampleCount;

        return averageColor;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="rayCastPair"></param>
    private void DrawHits(RayCastPair rayCastPair)
    {
        DrawHit(rayCastPair._leftHit);
        DrawHit(rayCastPair._rightHit);
        
        // store hit information for adding vertices in CuttingAndGeneration
        allLeftHits.Add(rayCastPair._leftHit);
        allRightHits.Add(rayCastPair._rightHit);
        
        canvasMaterial.SetTexture("_SecondTex", textureToColor);
    }
    
    private void DrawHit(RaycastHit hit)
    {
        Vector2 pixelUV = hit.textureCoord;
        
        pixelUV.x *= textureToColor.width;
        pixelUV.y *= textureToColor.height;

        List<Vector2> uvCoordinates = new List<Vector2>();

        // TODO: make it only one pixel wide. No need to send list.
        // iterate over square region centered around the pixel coordinate
        uvCoordinates.Add(pixelUV);

        /*
        int halfBrushSize = brushSize;
        for (int x = (int)pixelUV.x - halfBrushSize; x < (int)pixelUV.x + halfBrushSize; x++)
        {
            for (int y = (int)pixelUV.y - halfBrushSize; y < (int)pixelUV.y + halfBrushSize; y++)
            {
                // Ensure the pixel is within the texture bounds
                if (x >= 0 && x < textureToColor.width && y >= 0 && y < textureToColor.height)
                {
                    // Add pixel coordinate to list
                    uvCoordinates.Add(new Vector2(x , y ));
                }
            }
        }
        */

        // determine size for creating the Compute Buffer
        int numberOfUVs = uvCoordinates.Count;
        
        
        // put into dict for later usage
        if (!hullRaycastHits.ContainsKey(pixelUV))
        {
            hullRaycastHits.Add(pixelUV, hit);
            
            // add uvs to allDrawnPixels for calc GiftWrapping in CuttingAndGenerationClass
            allDrawnPixel.AddRange(uvCoordinates);
        }
        
        uvCoordinates.ToArray();
        
        // Set the UV coordinates as a ComputeBuffer
        ComputeBuffer uvBuffer = new ComputeBuffer(numberOfUVs, sizeof(float) * 2);
        uvBuffer.SetData(uvCoordinates);
        computeShader.SetBuffer(kernelID, "UVBuffer", uvBuffer);
        
        // Set the dimensions of the texture
        computeShader.SetInts("textureDimensions", textureToColor.width, textureToColor.height);
        
        // Dispatch the compute shader
        computeShader.Dispatch(kernelID, numberOfUVs, 1, 1);
        
        // Release the resources
        uvBuffer.Release();
    }
    
    
    
    /// <summary>
    /// Method to differentiate between two colors and get the difference
    /// </summary>
    /// <param name="color1"></param>
    /// <param name="color2"></param>
    /// <returns></returns>
    private float ColorDifference(Color color1, Color color2)
    {
        // Calculate the color difference as the Euclidean distance between the colors in RGB space
        var rDifference = color1.r - color2.r;
        var gDifference = color1.g - color2.g;
        var bDifference = color1.b - color2.b;

        var difference = Mathf.Sqrt(rDifference * rDifference + gDifference * gDifference + bDifference * bDifference);

        return difference;
    }
    
    // ===== ===== =====
    
    
    
    // ===== Statistical Calculations =====
    
    /// <summary>
    /// Calculates the cutting length of the wire after a hit
    /// </summary>
    /// <param name="hit"></param>
    /// <returns></returns>
    private float CalculateWireDepth(RaycastHit hit)
    {
        // get normal of hit
        Vector3 hitNormal = hit.normal;
            
        // get angle between vectors
        float angle = Vector3.Angle(transform.forward * -1, hitNormal);
            
        // get distance of hitpoint
        float hitpointDistance = hit.distance;
            
        // calc actual depth of wire
        float currentWireDepth = raylength - hitpointDistance;

        return currentWireDepth;
    }
    
    
    
    /// <summary>
    /// This calculates the integral limites (intersecions) of a parabola and a line
    /// the limits are changed as ref
    /// </summary>
    /// <param name="parabola"></param>
    /// <param name="line"></param>
    /// <param name="integrationLimit1"></param>
    /// <param name="integrationLimit2"></param>
    public void CalculateIntegralRangeQuadraticParabola(Polynomial parabola, Polynomial line, ref double integrationLimit1, ref double integrationLimit2)
    {
        // quadratic function: ax^2 + bx +c = 0
        float a = parabola.GetCoefficient(0);
        float b = parabola.GetCoefficient(1);
        float c = parabola.GetCoefficient(2);

        // coefficients of the line mx+b
        float m = line.GetCoefficient(0);

        // Discriminant of Midnight formula b^2 - 4 ac
        // Discriminant with a line cutting: b^2 -4 a * (c-m)
        float discriminant = b * b - 4f * a * (c - m);

        if (discriminant > 0f)
        {
            // two points 
            // x = (-b +- sqrt(D)) / 2a
            double x1 = (-b + Math.Sqrt(discriminant)) / (2f * a);
            double x2 = (-b - Math.Sqrt(discriminant)) / (2f * a);

            integrationLimit1 = x1;
            integrationLimit2 = x2;
        }
        else if (Math.Abs(discriminant) < Mathf.Epsilon)
        {
            // one point
            float x = -b / (2f * a);
        }
        else
        {
            // no point
        }
    }

    /// <summary>
    /// Calculating the Integral  of a Quadratic Function
    /// ∫[integrationLimit1, integrationLimit2] (ax^2 + bx + c) dx
    /// ∫(ax^2) dx = (a/3) * x^3 + C1
    /// ∫(bx) dx = (b/2) * x^2 + C2
    /// ∫c dx = cx + C3
    /// </summary>
    /// <param name="parabola"></param>
    /// <param name="integrationLimit1"></param>
    /// <param name="integrationLimit2"></param>
    /// <returns></returns>
    public double CalculateArea(Polynomial parabola, double integrationLimit1, double integrationLimit2)
    {
        // Extract coefficients of the quadratic equation
        double a = parabola.coefficients[0];
        double b = parabola.coefficients[1];
        double c = parabola.coefficients[2];

        // Calculate the definite integral of the quadratic equation
        double area = (a / 3.0) * (Math.Pow(integrationLimit2, 3) - Math.Pow(integrationLimit1, 3))
                      + (b / 2.0) * (Math.Pow(integrationLimit2, 2) - Math.Pow(integrationLimit1, 2))
                      + c * (integrationLimit2 - integrationLimit1);

        return Math.Abs(area);
    }
    
    

    private void SetCutInfo(float wireDepth, Vector3 wireTopPos)
    {
        if (storeFirstPosPair)
        {
            cutInfo1.wireDepth = wireDepth;
            cutInfo1.wireTip = wireTopPos;
            
            storeFirstPosPair = false;
        }
        else
        {
            cutInfo2.wireDepth = wireDepth;
            cutInfo2.wireTip = wireTopPos;
            
            storeFirstPosPair = true;
        }
    }



    /// <summary>
    /// Parabola: -1.5x^2 + 0x + 2
    /// Line: mx + n: we consider a straight line,
    /// 
    /// </summary>
    /// <returns></returns>
    private double CalcCorrectArea(float wireDepth)
    {
        // get wireDepth in mm
        float lineHeight = wireDepth * 100;

        // build line.
        Polynomial line = new Polynomial(new List<float>{Math.Abs(lineHeight)});

        // calc area of wire
        double integrationLimit1 = 0;
        double integrationLimit2 = 0;
        
        CalculateIntegralRangeQuadraticParabola(WireParabola, line, ref integrationLimit1, ref integrationLimit2);
        
        // before calculating the area, "move" the parabola down to only calc the area between the line, not to the
        // x axis
        
        //calc new c value:
        float newC = lineHeight;
        
        // create adaptedParabola
        Polynomial adaptedParabola = new Polynomial(new List<float> { WireParabola.GetCoefficient(0), WireParabola.GetCoefficient(1), newC });
        
        double wireArea = CalculateArea(adaptedParabola, integrationLimit1, integrationLimit2);
        
        //Debug.Log("Limits: " + integrationLimit1.ToString() + ", " + integrationLimit2.ToString());
        //Debug.Log("Area: " + wireArea.ToString("F8"));
        
        return wireArea;
    }

    /// <summary>
    /// Get two points from wire tip, calc area for each position, get middle value of both, multiply with distance between p1 and p2
    /// </summary>
    /// <returns></returns>
    public double CalcCorrectVolume(float wireDepth, Transform hitPos)
    {
        // set cutting info for current cut
        SetCutInfo(wireDepth, hitPos.position);
        
        // check if both cuttingInfo structs are not default
        if (!EqualityComparer<CutInfo>.Default.Equals(cutInfo1, default(CutInfo)) &&
            !EqualityComparer<CutInfo>.Default.Equals(cutInfo2, default(CutInfo)))
        {
            // calc area for each position
            double area1 = CalcCorrectArea(cutInfo1.wireDepth);
            double area2 = CalcCorrectArea(cutInfo2.wireDepth);
            
            // calc average
            double averageArea = (area1 + area2) / 2;

            // calc distance between the two tips
            double distance = Vector3.Distance(cutInfo1.wireTip, cutInfo2.wireTip);

            // distance to mm
            distance *= 100;
            
            // cacl volume with area x length
            double volume = averageArea * distance;
            
            // return volume as mm^3
            return volume;
        }
        return 0;
    }

    private void OnDisable()
    {
        
    }
}