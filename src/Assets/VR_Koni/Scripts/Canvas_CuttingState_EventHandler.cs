using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Canvas_CuttingState_EventHandler : MonoBehaviour
{
    private Transform mCanvas;

    private Transform mText;
    private TextMeshPro mTextComponent;
    
    // Start is called before the first frame update
    void Start()
    {
        mCanvas = transform.Find("Canvas");//transform.Find("Canvas");
        
        mText = mCanvas.GetChild(0);
        mTextComponent = mText.GetComponent<TextMeshPro>();

        ElectroSnareVR.onChangingState += UpdateText;
    }
    
    private void OnDisable()
    {
        ElectroSnareVR.onChangingState -= UpdateText;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void UpdateText(ElectroSnareVR.CuttingState cuttingState)
    {

        string textToDisplay = "";

        switch (cuttingState)
        {
            case ElectroSnareVR.CuttingState.NonCutting:
                mTextComponent.color = Color.blue;
                textToDisplay = "NonCutting";
                break;
            case ElectroSnareVR.CuttingState.HealthyTissue:
                mTextComponent.color = Color.green;
                textToDisplay = "Healthy";
                break;
            case ElectroSnareVR.CuttingState.CancerTissue:
                mTextComponent.color = Color.red;
                textToDisplay = "Cancer";
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(cuttingState), cuttingState, null);
        }
        
        mTextComponent.SetText(textToDisplay);
    }
}
