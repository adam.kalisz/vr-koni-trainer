using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine;
//[RequireComponent(typeof(CharacterController))]

public class Elektroschlinge_KeyboardDebug : MonoBehaviour
{

    [Header("Movement Parameters")] 
    [SerializeField] private float moveSpeed = 1.0f;

    [Header("Mouse Acceleration Parameters")]
    [SerializeField, Range(1, 10)] private float roationSensitivity = 0.5f;

    [Header("Bool Value to test if Draht is colliding with object")]
    //[ReadOnly(true)]
    [SerializeField] private bool cutting = false;

    /*
    [Header("Point of Wire")] [SerializeField]
    [Description("The wire of the medical instrument has a ...")]
    private Vector3 wirePoint;
    */

    [Header("CuttingDepth(LengthOfWire)")] 
    [SerializeField] private float raylength = 0.0186f;

    public const float wirelength = 0.0186f;
    
    
    //private CharacterController characterController;

    private Vector3 moveDirection;
    private Vector2 rotationDirection;

    private Rigidbody mRigidbody;

    private Collider m_WireCollider;

    private Transform mainRayCast;
    
    
    private void Awake()
    {
        //characterController = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Fetch the Rigidbody from the GameObject with this script attached
        mRigidbody = GetComponent<Rigidbody>();
        
        // Get the child component using GetComponent
        Transform child = transform.Find("Draht");

        /*
        // Check if the child component is found
        if (child != null)
        {
            // Use the child component
            Debug.Log("CHILD FOUND!!!");

            m_WireCollider = child.GetComponent<BoxCollider>();

        }
        */

        mainRayCast = transform.Find("RayCastOrigin");



    }

    // Update is called once per frame
    void Update()
    {
        HandleMovementInput();
        HandleRotationInput();

        ApplyFinalMovements();
    }

    private void FixedUpdate()
    {
        RaycastHit hit;
        // https://docs.unity3d.com/ScriptReference/Physics.Raycast.html
        
        // get raycast hit of middle point of the instrument. Use the angle between the normal and the forward vector of 
        // the instrument and the length of the raycast to determine, how deep the wire cuts into the tissue
        if (Physics.Raycast(mainRayCast.position, mainRayCast.TransformDirection(Vector3.forward), out hit, raylength))
        {
            // get normal of hit
            Vector3 hitNormal = hit.normal;
            
            // get angle between vectors
            float angle = Vector3.Angle((transform.forward * -1), hitNormal);
            
            // get distance of hitpoint
            float hitpointDistance = hit.distance;
            
            // calc actual depth of wire
            float wireDepth = wirelength - hitpointDistance;
            
            Debug.Log("Tiefe: " + (wireDepth * 100) + " mm");
            Debug.Log("Winkelabweichung: " + angle + " Grad");
            
            Debug.DrawRay(hit.point, hit.normal * 100, UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
            
            Debug.DrawRay(mainRayCast.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");
            LeaveTrail(hit.point, 0.001f);
        }
        
    }


    private void HandleMovementInput()
    {
        float movementX = Input.GetAxis("Horizontal");
        float movementY = Input.GetAxis("UpDown");
        float movementZ = Input.GetAxis("Vertical");

        moveDirection = new Vector3(movementX, movementZ, movementY);
        moveDirection = Vector3.ClampMagnitude(moveDirection, 1);
    }

    private void HandleRotationInput()
    {
        float rotationX = Input.GetAxis("Mouse Y");
        float rotationY = 0;
        float rotationZ = Input.GetAxis("Mouse X");

        Vector3 axis = new Vector3(rotationX, rotationY, rotationZ); 
        // manipulate rigidbody instead of transform
            //transform.Rotate(axis, 180*Time.deltaTime);
            
       // define eulerangle x,y,z degrees     
       Vector3 tEulerAnlgeVelocity = new Vector3(rotationX, rotationY, rotationZ); 
       Quaternion deltaRotation = Quaternion.Euler(tEulerAnlgeVelocity * Time.deltaTime);
       mRigidbody.MoveRotation(mRigidbody.rotation * deltaRotation);
    }
    
    private void ApplyFinalMovements()
    {
        // manipulate rigidbody instead of transform
            //transform.Translate(moveSpeed * Time.deltaTime * moveDirection);
        //m_Rigidbody.MovePosition(transform.position + moveSpeed * Time.deltaTime * moveDirection );      
        mRigidbody.MovePosition(mRigidbody.position + moveSpeed * Time.deltaTime * moveDirection );     
    }


    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("TRIGGER!!!");
    }

    // unnesseccary. i can always stop the velocity and only allow the input. 
    // drawing happens on koni side
    private void OnCollisionEnter(Collision otherCollider)
    {
        //Debug.Log("COLLISION!");
        
        mRigidbody.MovePosition(mRigidbody.position);
        mRigidbody.MoveRotation(mRigidbody.rotation);
        
        
        mRigidbody.isKinematic = true;

        ContactPoint[] contactPoints = otherCollider.contacts;
        /*
        foreach (ContactPoint contact in contactPoints)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = contact.point;
            sphere.transform.localScale = new Vector3(0.0001f, 0.0001f, 0.0001f);
        }
        */
        //Debug.Log("---with: " + otherCollider.collider.GetComponent<BoxCollider>().name );

        
        // Make an empty list to hold contact points
        ContactPoint[] contacts = new ContactPoint[10];

        // Get the contact points for this collision
        int numContacts = otherCollider.GetContacts(contacts);

        /*
        foreach (ContactPoint contact in contacts)
        {
            LeaveTrail(contact.point, 0.001f );
        }
        */

        /*
        foreach (ContactPoint contact in contacts)
        {
            LeaveTrail(m_WireCollider.bounds.center , 0.001f );
        }
        */
        
        // Print how many points are colliding with this transform
        //Debug.Log("Points colliding: " + otherCollider.contacts.Length);

        // Print the normal of the first point in the collision.
        //Debug.Log("Normal of the first point: " + otherCollider.contacts[0].normal);

        // Draw a different colored ray for every normal in the collision
        foreach (var item in otherCollider.contacts)
        {
            //Debug.DrawRay(item.point, item.normal * 100, UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
        }


        /*
        foreach (ContactPoint contact in otherCollider.contacts)
        {
            Collider colliderReason = otherCollider.GetContact(0).thisCollider;
            Debug.Log("Collision with: " + otherCollider.collider.name + " and: " + colliderReason.name);
            
            
            //Debug.Log("point: " + contact.thisCollider.gameObject.name);
            
            //Debug.DrawRay(contact.point, contact.normal, Color.red);
        }
        */
    }


    private void OnCollisionStay(Collision other)
    {
        
        //Debug.Log("STAY");
        
        
        
        
    }

    private void OnCollisionExit(Collision other)
    {   
        mRigidbody.isKinematic = false;
        mRigidbody.MovePosition(mRigidbody.position);
        mRigidbody.MoveRotation(mRigidbody.rotation);
        
        //Debug.Log("EXXXXXIT!");
        
        //throw new NotImplementedException();
    }
    
    
    /// <summary>
    /// Places a single sphere at a specific point in space, and sets it to auto-destroy
    /// </summary>
    /// <param name="point">The world point at which to spawn the sphere</param>
    /// <param name="scale">The local scale of the sphere</param>
    /// <param name="material">The material to apply to the sphere</param>
    private void LeaveTrail(Vector3 point, float scale)//, Material material)
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.localScale = Vector3.one * scale;
        sphere.transform.position = point;
        sphere.transform.parent = transform.parent;
        sphere.GetComponent<Collider>().enabled = false;
        //sphere.GetComponent<Renderer>().material = material;
        Destroy(sphere, 3f);
    }

}
