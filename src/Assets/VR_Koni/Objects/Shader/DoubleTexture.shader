Shader "Custom/DoubleTexture"
{
    Properties
    {
        _Color ("Color", Color) = (1, 1, 1, 1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _SecondTex ("Second Texture", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0, 1)) = 0.5
        _Metallic ("Metallic", Range(0, 1)) = 0.0
        // TODO: add custom property to allow input of multiple uv coordinates via a c# script
        _CustomUVs ("Custom UV Coordinates", Vector) = (-1, -1, -1, -1)
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows

        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _SecondTex;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_SecondTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        //float4 _CustomUVs; // Custom UV coordinates

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        /*
        // Function to check if UV coordinates match any of the custom UVs
        bool CustomUVMatches(float2 uv)
        {
            return (uv.x == _CustomUVs.x && uv.y == _CustomUVs.y) ||
                   (uv.x == _CustomUVs.z && uv.y == _CustomUVs.w);
        }
        */
        
        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 mainColor = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            fixed4 secondColor = tex2D(_SecondTex, IN.uv_SecondTex);

            /*
            // TODO: compare pixel to paint with pixels of _SecondTex. if equal, paint those pixels on the secondTex
            if (CustomUVMatches(IN.uv_SecondTex))
            {
                // TODO: paint uv pixels of secondTex
                // Set the color you want to paint with
                fixed4 customPaintColor = fixed4(0.0, 1.0, 0.0, 1.0); // Red color, for example
                
                secondColor = customPaintColor;
            }
            */
            
            // Use the opacity from the second texture to determine if the pixel is opaque
            if (secondColor.a > 0)
            {
                mainColor = secondColor;
            }
            
            o.Albedo = mainColor.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = mainColor.a;
        }

        
        
        ENDCG
    }
    FallBack "Diffuse"
}
