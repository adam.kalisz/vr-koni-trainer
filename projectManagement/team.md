# ☯ Teamwork

- We don't know the solution either, but we surely have an idea. Use our experience and discuss alternatives with us, ...
- Team != Great, someone else does it (literally 'Toll Ein Anderer Machts')
  - 'I have done my job' is not enough!
- Use the time on site effectively
- Work effectively -- you will be more effective if you do not spend hours with trial and error, but e.g. ask for help after 30 min. At night and on weekends it is more difficult ...
- Keep commitments and communicate delays/problems/obstacles as early as possible.
- [Tuckman Phase Model](https://teamentwicklung-lab.de/tuckman-phasenmodell/) (in German)
- [Belbin team roles: diversity not diversity](https://karrierebibel.de/belbin-teamrollen/) (in German)

## Communication

The team meets on Wednesday at the h_da and an additional day during the week without the lecturers (Fri if possible). Communication with the physicians will only occur after consultation with the lecturers.

Discord:

- Address conflicts early and in an appreciative manner and resolve asap.
- use retros

### Sources for better communication

- [Systemic Questions](https://www.business-wissen.de/hb/beispiele-fuer-systemische-fragen/) (in German)
- [Feedforward](https://arras-consulting.de/feedback-oder-feedforward/) (in German)
- [Transactional Analysis](https://karrierebibel.de/transaktionsanalyse/) (in German)
- [Communication Square](https://www.schulz-von-thun.de/die-modelle/das-kommunikationsquadrat) (in German)

## Stakeholders

- Medical expertise: Anne Cathrine Quenzer and Matthias Kiesel
- Blender expert: Adam Kalisz
- Graphics/Unity: Benjamin Meyer
- C#/application context/coaching Scrum Master: Ute Trapp

## Scrum Team

You work as a cross-functional team.

- [Scrumguide](https://scrumguide.de/)
- Scrum Master (<50% of working time) moderates and prepares meetings, ensures issues are estimable and prioritized, clarifies problems, documents processes appropriately.
- Product Owner (<30% of working time) formulates the feature issues and prioritizes them in consultation with the lecturers.
- Scrum Master and Product Owner ensure regular grooming.
- The team formulates non-feature issues, works on the issues, contributes ideas and addresses problems independently. At review, the team presents the increment and discusses alternatives in meetings.

## Evaluation criteria

- Communication skills
- Team ability
- Qualitative processing of story points, use weight in issues and planing poker to determine the weight
- Active advancement of the project
- Ability to learn and critical faculties
- Concentrated effective way of working
- Professional use of gitlab etc.
