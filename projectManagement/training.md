# Familiarization - Sprint 0

In order for everyone to launch at full speed, a good and solid induction is necessary. 
According to the 7.5 ECTS and a 42 time hours week you have therefore approx. 10.5 time hours per week available for the project -- for averagely talented students with a solid programming education according to the bachelor degree. 
You have three weeks for training, i.e. you will have about 32 time hours available for this. Create an issue, assign yourself and work through the following tutorials/steps. 
For the training we estimate approx. 28h and for the preparation of the presentation approx. 4h.

- Installation (1h)
- [ ] [Rider](https://www.jetbrains.com/rider/) from Jetbrains (students of the hda get a free version)
- [ ] [Unity](https://unity.com/download), [LTS Version 2022.3.11](https://unity.com/releases/lts), development with C#
- [ ] [Rider - Unity](https://www.jetbrains.com/help/rider/Unity.html) and [tips](https://unity.com/how-to/tips-optimize-jetbrains-rider-cross-platform-c-script-editor-game-developers)
  - [ ] Alternatively [VS Code](https://code.visualstudio.com/download)
    - [Video-Tutorial](https://www.youtube.com/watch?v=ihVAKiJdd40)
- [ ] [Blender](https://www.blender.org/download/), [LTS Version 3.6.4](https://www.blender.org/download/lts/3-6/)
- Tutorial C# (approx. 3h)
  - [general for reference](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/)
  - [Basic Tutorial with exercises](https://www.w3schools.com/cs/index.php)
  - [coding conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions)
  - [ ] delegates
    - [delegates in C# in general](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/delegates/)
    - [Delegates in a Unity example](https://gamedevbeginner.com/events-and-delegates-in-unity/)
  - [ ] [lambdas](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/operators/lambda-expressions)
  - [ ] async
    - [Async in C# general](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/async/)
    - [ ] [async in Unity](https://docs.unity3d.com/2021.2/Documentation/Manual/JobSystem.html)
  - [ ] [Design Patterns](https://resources.unity.com/games/level-up-your-code-with-game-programming-patterns?ungated=true)
    - factory
    - state
    - observer
    - MVP
  - [ ] [Head First C# - Unity Labs, Rider Edition](https://github.com/head-first-csharp/fourth-edition/blob/master/Unity_Labs/Unity_Labs_Rider_Edition.pdf)
- [ ] Familiarization with Blender (approx. 2h), optional!

  - https://www.youtube.com/watch?v=nIoXOplUvAw
  - https://www.youtube.com/watch?v=imdYIdv8F4w

- Familiarization with Unity (approx. 3h)
  - [ ] [Unity book](https://resources.unity.com/games/unity-game-dev-field-guide?ungated=true), just read across for orientation
  - [ ] [Unity Essentials](https://learn.unity.com/pathway/unity-essentials)
  - [ ] [Unity scripting C#](https://dev.to/codemaker2015/unity-3d-c-scripting-cheatsheet-for-beginners-34f8)
  - [ ] pick one of the lessons of [Creative tools for artists and designers](https://unity.com/solutions/artist-designers)
- Introduction to Unity-VR (approx. 3h)
  - [ ] [create with vr](https://learn.unity.com/course/create-with-vr)
  - [ ] [Unity Manual](https://docs.unity3d.com/Manual/VROverview.html)

  - Setup VR with OpenXR
    - [ ] [VR Setup](https://www.youtube.com/watch?v=F4gDjCLYX88&list=PLwz27aQG0IIK88An7Gd16An9RrdCXKBAB&index=13)
    - [ ] [VR Interactables](https://www.youtube.com/watch?v=furfe8E7SOA&list=PLwz27aQG0IIK88An7Gd16An9RrdCXKBAB&index=11)
    - [ ] [Playlist VR Basics](https://www.youtube.com/playlist?list=PLX8u1QKl_yPD4IQhcPlkqxMt35X2COvm0)

  - Setup VR with SteamVR
    - [ ] [Valve Index in Unity](https://valvesoftware.github.io/steamvr_unity_plugin/)
    - [ ] [Quickstart / Download](https://valvesoftware.github.io/steamvr_unity_plugin/articles/Quickstart.html)
    - [ ] [First Steps / Getting Started](https://valvesoftware.github.io/steamvr_unity_plugin/articles/Interaction-System.html)

- Create your own VR project (approx. 16h) with the following requirements
  - [ ] Use of an asset created in Blender
  - [ ] Control of a tool
  - [ ] Collision detection between tool and asset
  - [ ] Integration of useful freely available assets
  - [ ] Implementation of a functionality we will need later on

## Presentations on 01.11. and 08.11.

Divide the [Best Pratices](https://docs.unity3d.com/2019.3/Documentation/Manual/BestPracticeGuides.html) between each other and prepare short presentations, distributed on the two dates. Create and assign an issue for this as well. Alternatively, you can work on one of the prepared issues with presentation.
Special topics of interest
- [UI](https://resources.unity.com/games/user-interface-design-and-implementation-in-unity?ungated=true)
- [Scriptable Objects](https://resources.unity.com/games/create-modular-game-architecture-with-scriptable-objects-ebook?ungated=true)
- [Automatic Tests](https://unity.com/how-to/automated-tests-unity-test-framework)
- [Tips](https://learn.unity.com/tutorial/unity-tips#64622ce0edbc2a32a219b25b)