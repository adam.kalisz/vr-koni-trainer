# Retros
Record the key points of your retros here.

## **Example entry** 30.11.22
- Participants: all
- Problems
  1. merge resulted in a night shift
  2. ...
- Action-Items (Problem - Action-Item)
 - to 1.: Everyone has to make their MRs until Sunday 18:00 clock 
 - to 1.: On the Daily on Friday before, the big changes are already merged, i.e. on Wednesday before it is determined what  must be completed by then.
