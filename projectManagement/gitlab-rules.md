# Gitlab usage

- Keep board up to date
- Pushing to the directory with the actual program code (src) only with PRs, 
  - simple doc changes, presentations, schedules etc. can be pushed directly to the main (again, please
  consider the commit comments)
- Language
  - gitlab comments, program code completely in English
  - issues, comments in English

## Workflow

1. Move issue to _doing_.

- Document progress via tasks
- Track effort via spent, cf. [time tracking](https://docs.gitlab.com/ee/user/project/time_tracking.html)

2. Create branch
3. Implement feature and tests, pushing after each implementation unit
4. Clean up code, code inspection etc.
5. Merge main into own branch
   - Just run `git merge main` on the corresponding branch.
   - Otherwise also possible in the GitLab web application (as described)
6. Move issue to _needs review_.
7. Perform code review with another person -- this is more like pair programming and a precursor to a merge request
   a precursor to the merge request
8. Make merge request (use template and put _Draft:_ in title)
9. In the first sprint each merge is checked by one of the lecturers and merged as soon as everything is ok (later merges are done by team members)
   - If something is not right, it will be noted in the code or in a comment in the merge request. 
10. After a successful merge request move issue to _Sprint Review_ -- only issues listed there will be shown in the Sprint Review.

## Naming

- File and directory names in English and only the characters [a-zAZ0-9_-].
- commit comments -- see. https://www.conventionalcommits.org/en/v1.0.0/
- branches
  - structure: feat/fix/docs/style/refactor/learn/orga/test / #issue Titel
  - example _feat-49-adds-new-endpoints-to-fast-api_
- merge request
  - structure #Issue title
  - example _#49 adds new endpoints to FastAPI_
  
Once the issue number is included, it appears as a comment in the issue, i.e. you can see the commits and activities in the issue.

## Helpful links

- https://docs.gitlab.com/ee/development/code_review.html
- https://www.gitkraken.com/blog/code-review
- https://blog.codacy.com/how-to-code-review-in-a-pull-request/
- https://www.pullrequest.com/blog/11-ways-to-create-pull-requests-that-are-easy-to-review/
