# Meetings

Record the key points of your meetings here.

## **Example entry** 17.10.22: Weekly

- ToDos
  - Jonas implements the tests until Monday
  - Alex merges all open MRs until Monday
  - Everyone creates possible MRs until Mo 8:00
- Open problems
  - camera moves jerkily
- Topics: Problems with camera, preparation sprint review
