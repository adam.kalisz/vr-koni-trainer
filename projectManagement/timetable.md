# 📆 Schedule WiSe 23/24 (16.10.23 - 9.2.24)

The team will meet on site every week on Wednesdays (see OBS).

## Sprints

We plan with three-week sprints with a Sprint 0 for familiarization and a very shortened sprint at the end for project completion.

### Sprint 0 (16.10. - 03.11.): Familiarization

- Everyone is familiar with Unity, Blender, VR and the project goal according to [training](training.md)
- Presentations according to [training](training.md) were held
- Dates for interim and final presentation have been set in consultation with stakeholders and team
- Team agreed on a fixed date for another weekly 
- Alternative dates for the holidays have been coordinated with lecturers and the team.
- the issues for Sprint 1 are formulated and discussed with stakeholders
- Issues for the further sprints are roughly formulated

### Sprint 1 (06.11. - 24.11.): Architecture/assets/first features/CI

- The basic architecture is in place and understood by everyone
- Essential assets have been found/created and added to the project
- The first relevant features have been implemented
- Spikes for Sprint 2 edited
- CI pipeline set up (nice to have)

### Sprint 2 (27.11. - 15.12.): MVP

- A first increment with the main features is executable and can be shown in the interim presentation and made available to stakeholders to try out.

### Sprint 3 (18.12.23 - 26.1.24): Fine Tuning

- Stakeholder feedback is incorporated
- Cleanup/Refactoring
- Bugs fixed
- Fine Tuning, beta version is ready

### Sprint 4 (29.1.24 - 9.2.24): Handover/Documentation

- Workflow, architecture, major components are documented

## Individual appointments

At the beginning, all lecturers are present (weekly), thereafter one each. 

### 18.10.23

- 10:15: Short introduction of the project and the accompanying team; clarification of questions (Ute).
- 10:30: Rules of the game, general conditions (Ute)
- 10:45: Team building
- 11:15: short break
- 11:30: master students describe their focus and needs
- 12:00: gitlab usage (Ute)
- 12:10: short break
- 12:20: basics Unity (Benjamin)
- 13:10: Clarification of tasks until next week, see [training.md](training.md) (Ute)
- 13:20: organize access to Valve Index
- 13:30: end

### 25.10.23

- 10:15: Teambuilding -> in the teams (Ute).
- 11:00: Current status of prototype, + test of VR glasses (Wilpert)
- 12:00: Break
- 12:15: Organization access VR-Goggles
- 12:30: Current status of requirements (Benjamin)
- 13:15: Planning next week (presentations, next meetings in the teams)

### 01.11.23

- 10:15 Ankommen und alle schauen sich die aktuellen Projekte der anderen an
- 10:45 Input Scrum Master
- 11:00 in einzelnen Teams: Aktivität
- 11:15 Scheib zu CI/CD
- 11:30 Annika zu Designing with Unity UI
- 11:45 Pause
- 12:00 Philipp zu besonderen Aspekten in der Entwicklung mit Unity
- 12:15 Besprechung in der Gruppe bzgl. Orga
- 12:30
    - Pair-Programming an den individuellen Projekten oder konkrete Projekte genauer ansehen, teamübergreifend;
    - parallel: Vorbereitung Treffen Würzburg: POs, Trapp, Abu Elnaser, Kemmerer, ???
- 13:00 Priorisierung Etherpad/Architektur
- 13:30 Ende

### 08.11.23

- 10:15 Ankommen und vorbereiten der ersten Präsentation
- 10:20 Anne und Emil zu den Ergebnissen des Würzburgtermins
- 10:35 Duc zu Nützliche Tools für Entwicklung in Unity
- 10:50 Kay zu Best Practice für Memory Management
- 11:05 Energizer / Pause (10 min)
- 11:15 Marc zu Unity UI Toolkit
- 11:30 Shawkat zu Unity UI Layout
- 11:45 Pause (15 min)
- 12:00 Jonas zu Zusammenstellung kleiner NiceToKnow / BestPractices
- 12:15 Pierre zu Figma for Unity UI
- 12:30 Husey zu Delegates und Events in Unity
- 12:45 Cagdas zu Design Patterns
- 13:00 je nach Möglichkeit einer der folgenden Punkte:
    - *Vorstellung der Ergebnisse Expertengruppe Unity des Architektur Teams*
    - Sprintplanung
- 13:30 Ende

### 29.11.23 

- Sprint Review
- Retrospective
- Sprint planning
- Project architecture review

### 20.12.23 

- Sprint review
- Retrospective
- Sprint Planning
- Project architecture review

### 31.01.24 

- Sprint review
- Retrospective
- Sprint Planning
- Project architecture review