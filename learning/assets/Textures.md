# Textures

## A few words regarding texture formats in Unity

### Normal Maps
Unity uses **Y+/OpenGL** normal maps. If theres a **Y-/DirectX** normal map, simply invert the green-channel to make it work in unity.

### Roughness and Glossyness
There are two ways of representing how "rough/shiny" a surface should look. 
- **Roughness**: 0.0 = 100% glossy, 1.0 = 100% rough
- **Glossyness**: 0.0 = 100% rough, 1.0 = 100% glossy

One can easily **convert** them into the other by **inverting the brightness** of the texture/value. 

## Here are some great websites for Textures with licences we could use
- [Texture Haven](https://polyhaven.com/textures), a lot of PBR textures
- [HDRI Haven](https://hdri-haven.com/), HDRI environment textures
- [Free PBR](https://freepbr.com/about-free-pbr/), even more PBR textures
