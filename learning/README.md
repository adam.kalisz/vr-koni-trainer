# Learning

Collect sample code here to familiarize with or try out individual technologies/libraries. Add helpful tutorials, snippets, etc. to this document.

## Tutorials

## Snippets

## Assets
Inside /[assets](https://gitlab.com/utrapp/vr-koni-trainer/-/tree/main/learning/assets?ref_type=heads) one can find useful links to assets and other sources that may help when creating.

## Presentations
Inside /[presentations](https://gitlab.com/utrapp/vr-koni-trainer/-/tree/main/learning/presentations?ref_type=heads) one can find the .pdf-files of the students presentations. 

## ???
